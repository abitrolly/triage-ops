# frozen_string_literal: true

require_relative '../lib/timeframe_helper'

Gitlab::Triage::Resource::Context.include TimeframeHelper
