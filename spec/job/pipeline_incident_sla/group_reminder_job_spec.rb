# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/job/pipeline_incident_sla/group_reminder_job'
require_relative '../../../triage/triage/event'
require_relative './pipeline_incident_sla_job_shared_context'

RSpec.describe Triage::PipelineIncidentSla::GroupReminderJob do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        label_names: label_names,
        url: 'incident_web_url'
      }
    end
  end

  include_context 'with pipeline incident sla job'

  let(:label_names) { ['group::pipeline security'] }
  let(:updated_at)  { '2024-04-15T05:50:00.000Z' } # 10 minutes ago

  describe '#perform' do
    context 'when incident is not attributed to any group' do
      let(:label_names) { ['master:broken'] }

      it 'does nothing' do
        expect_no_request { subject.perform(event) }
      end
    end

    context 'when incident is closed' do
      let(:state) { 'closed' }

      it 'does nothing' do
        expect_no_request { subject.perform(event) }
      end
    end

    context 'when incident has been updated within the last 10 minutes' do
      let(:five_minutes_ago) { Time.now - (5 * 60) }
      let(:updated_at) { five_minutes_ago.strftime("%Y-%m-%d %H:%M:%S") }

      it 'does nothing' do
        expect_no_request { subject.perform(event) }
      end
    end

    context 'when incident has not been updated within the last 10 minutes' do
      let(:slack_client_double) { instance_double(Slack::Messenger) }
      let(:expected_text) do
        <<~MESSAGE.chomp
          This channel is the triage DRI for pipeline incident <incident_web_url|#456>. Please take action following the <https://handbook.gitlab.com/handbook/engineering/workflow/#triage-dri-responsibilities|Broken master handbook page>!
          I will ping the channel again in 50 minutes of inactivity. Thanks for your attention!
        MESSAGE
      end

      let(:webhook) { 'slack_webhook_url' }

      before do
        stub_env('SLACK_WEBHOOK_URL', webhook)
        allow(Slack::Messenger).to receive(:new).with(
          webhook,
          { channel: 'g_pipeline-security_alerts',
            username: 'gitlab-bot',
            icon_emoji: ':gitlab-bot:' }
        ).and_return(slack_client_double)
      end

      it 'sends a Slack reminder to the group' do
        expect(slack_client_double).to receive(:ping).with(text: expected_text)

        subject.perform(event)
      end
    end
  end
end
