# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/job/pipeline_incident_sla/stage_warning_job'
require_relative '../../../triage/triage/event'
require_relative './pipeline_incident_sla_job_shared_context'

RSpec.describe Triage::PipelineIncidentSla::StageWarningJob do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        label_names: label_names,
        url: 'incident_web_url'
      }
    end
  end

  include_context 'with pipeline incident sla job'

  let(:label_names) { ['group::pipeline security'] }
  let(:updated_at)  { '2024-04-15T04:00:01.000Z' } # 1 hour 59 minutes 59 seconds ago

  describe '#perform' do
    context 'when incident is not attributed to any group' do
      let(:label_names) { ['master:broken'] }

      it 'does nothing' do
        expect_no_request { subject.perform(event) }
      end
    end

    context 'when incident is closed' do
      let(:state) { 'closed' }

      it 'does nothing' do
        expect_no_request { subject.perform(event) }
      end
    end

    context 'when incident has been updated within the last 1.5 hours' do
      let(:five_minutes_ago) { Time.now - (5 * 60) }
      let(:updated_at) { five_minutes_ago.strftime("%Y-%m-%d %H:%M:%S") }

      it 'does nothing' do
        expect_no_request { subject.perform(event) }
      end
    end

    context 'when incident has not been updated within the last 1.5 hours' do
      let(:slack_client_double) { instance_double(Slack::Messenger) }
      let(:channel) { 'stage_with_two_groups_channel' }
      let(:expected_text) do
        <<~MESSAGE.chomp
          I'm requesting help on behalf of `#g_pipeline-security_alerts` to triage pipeline incident <incident_web_url|#456>.
          The incident has not received any update and will be escalated to <##{described_class::DEV_ESCALATION_CHANNEL_ID}> in 30 minutes of inactivity (except weekends and holidays).
          Thanks for your attention!
        MESSAGE
      end

      let(:webhook) { 'slack_webhook_url' }

      before do
        stub_env('SLACK_WEBHOOK_URL', webhook)
        allow(Slack::Messenger).to receive(:new).with(
          webhook,
          { channel: channel,
            username: 'gitlab-bot',
            icon_emoji: ':gitlab-bot:' }
        ).and_return(slack_client_double)
      end

      it 'sends a Slack reminder to the stage channel' do
        expect(slack_client_double).to receive(:ping).with(text: expected_text)

        subject.perform(event)
      end

      context 'when stage channel is not found' do
        let(:label_names) { ['Contributor Success'] }
        let(:channel) { 'development' }
        let(:expected_text) do
          <<~MESSAGE.chomp
            I'm requesting help on behalf of `#contributor-success` to triage pipeline incident <incident_web_url|#456>.
            The incident has not received any update and will be escalated to <##{described_class::DEV_ESCALATION_CHANNEL_ID}> in 30 minutes of inactivity (except weekends and holidays).
            Thanks for your attention!
          MESSAGE
        end

        it 'pings #development' do
          expect(slack_client_double).to receive(:ping).with(text: expected_text)

          subject.perform(event)
        end
      end

      context 'when stage is not found' do
        let(:label_names) { ['group::gitaly::git'] } # mocked stages.json is missing the systems stage
        let(:channel) { 'development' }
        let(:expected_text) do
          <<~MESSAGE.chomp
            I'm requesting help on behalf of `#g_gitaly` to triage pipeline incident <incident_web_url|#456>.
            The incident has not received any update and will be escalated to <##{described_class::DEV_ESCALATION_CHANNEL_ID}> in 30 minutes of inactivity (except weekends and holidays).
            Thanks for your attention!
          MESSAGE
        end

        it 'pings #development' do
          expect(slack_client_double).to receive(:ping).with(text: expected_text)

          subject.perform(event)
        end
      end

      context 'when stage should be excluded' do
        let(:label_names) { ['Engineering Productivity'] }
        let(:channel) { 'development' }
        let(:expected_text) do
          <<~MESSAGE.chomp
            I'm requesting help on behalf of `#g_engineering_productivity` to triage pipeline incident <incident_web_url|#456>.
            The incident has not received any update and will be escalated to <##{described_class::DEV_ESCALATION_CHANNEL_ID}> in 30 minutes of inactivity (except weekends and holidays).
            Thanks for your attention!
          MESSAGE
        end

        it 'pings #development' do
          expect(slack_client_double).to receive(:ping).with(text: expected_text)

          subject.perform(event)
        end
      end
    end
  end
end
