# frozen_string_literal: true

RSpec.shared_context 'with pipeline incident sla job' do
  let(:issue_path)  { "/projects/#{project_id}/issues/#{event.iid}" }
  let(:state)       { 'opened' }
  let(:issue_attr) do
    { 'iid' => iid, 'state' => state, 'labels' => label_names, 'updated_at' => updated_at }
  end

  subject { described_class.new }

  around do |example|
    Timecop.freeze(Time.utc(2024, 4, 15, 6)) { example.run } # 2024-04-15T06:00:00.000Z
  end

  before do
    stub_api_request(path: issue_path, response_body: issue_attr)
  end
end
