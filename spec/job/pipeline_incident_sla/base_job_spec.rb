# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/job/pipeline_incident_sla/base_job'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::PipelineIncidentSla::BaseJob do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        label_names: label_names,
        url: 'incident_web_url'
      }
    end
  end

  subject { described_class.new }

  describe "#threshold_in_seconds" do
    it "raises NotImplementedError" do
      expect { subject.send(:threshold_in_seconds) }.to raise_error(NotImplementedError)
    end
  end

  describe "#slack_channel" do
    it "raises NotImplementedError" do
      expect { subject.send(:slack_channel) }.to raise_error(NotImplementedError)
    end
  end

  describe "#slack_message" do
    it "raises NotImplementedError" do
      expect { subject.send(:slack_message) }.to raise_error(NotImplementedError)
    end
  end
end
