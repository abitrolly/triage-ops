# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/infradev_issue_label_nudger_job'
require_relative '../../triage/triage/event'
require_relative '../../triage/triage/infradev_issue_label_validator'

RSpec.describe Triage::InfradevIssueLabelNudgerJob do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      { from_gitlab_org?: true,
        label_names: label_names }
    end
  end

  let(:validator_double) { double('Triage::InfradevIssueLabelValidator') }

  let(:request_body) do
    <<~MARKDOWN.chomp
      <!-- triage-serverless InfradevIssueLabelNudger -->
      :wave: @joe - This ~"infradev" issue is missing a group label and is at risk of missing its SLO due date.

      Please apply a group label now following the [Triage Process](https://about.gitlab.com/handbook/engineering/workflow/#triage-process) handbook page section.

      You can ping one of the [Engineering Directors](https://about.gitlab.com/handbook/engineering/development/#managers-and-directors) if you require assistance.

      *This message was [generated automatically](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations).
      You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/job/infradev_issue_label_nudger_job.rb).*
    MARKDOWN
  end

  let(:issue_path)        { "/projects/#{project_id}/issues/#{iid}" }
  let(:issue_label_names) { ['infradev'] }
  let(:issue_state)       { 'opened' }

  let(:issue_attr) do
    {
      'project_id' => project_id,
      'iid' => iid,
      'labels' => issue_label_names,
      'state' => issue_state
    }
  end

  before do
    stub_api_request(path: issue_path, response_body: issue_attr)
  end

  describe '#perform' do
    context 'when infradev issue is open and is missing group label' do
      it 'pings username in a nudge comment for adding group label' do
        expect_comment_request(event: event, body: request_body) do
          subject.perform(event)
        end
      end
    end

    context 'when issue is closed' do
      let(:issue_state) { 'closed' }

      it 'does nothing' do
        expect_no_request(
          verb: :post,
          request_body: { body: request_body },
          path: "/projects/#{project_id}/issues/#{iid}/notes",
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end

    context 'when issue has no infradev label' do
      let(:issue_label_names) { [''] }

      it 'does nothing' do
        expect_no_request(
          verb: :post,
          request_body: { body: request_body },
          path: "/projects/#{project_id}/issues/#{iid}/notes",
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end

    context 'when issue has both infradev and group label' do
      let(:issue_label_names) { ['group::group1', 'infradev'] }

      it 'does nothing' do
        expect_no_request(
          verb: :post,
          request_body: { body: request_body },
          path: "/projects/#{project_id}/issues/#{iid}/notes",
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end
  end
end
