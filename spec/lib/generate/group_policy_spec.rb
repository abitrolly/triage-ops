# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/generate/group_policy'

RSpec.describe Generate::GroupPolicy do
  let(:options) { double(:options, template: template_path, only: only) }
  let(:only) { nil }
  let(:template_path) { 'path/to/template' }
  let(:template_name) { File.basename(template_path) }
  let(:output_dir) { "#{described_class.destination}/#{template_name}" }

  before do
    expect(File)
      .to receive(:read)
      .with(template_path)
      .and_return(<<~ERB)
        <%= group.key %>
        <%= group.label %>
      ERB

    expect(FileUtils)
      .to receive(:rm_r)

    expect(FileUtils)
      .to receive(:mkdir_p)
      .with(output_dir)
  end

  describe '.run' do
    shared_examples 'generates and writes policy files' do
      it 'generates' do
        expect(File)
          .to receive(:write)
          .with(
            "#{output_dir}/group1.yml",
            <<~MARKDOWN)
              group1
              group::group1
            MARKDOWN

        # The following groups have triage_ops_config.ignore_templates set to 'path/to/template'
        expect(File)
          .not_to receive(:write)
          .with("#{output_dir}/group2.yml", anything)
        expect(File)
          .not_to receive(:write)
          .with("#{output_dir}/group3.yml", anything)

        described_class.run(options)
      end
    end

    it_behaves_like 'generates and writes policy files'

    context 'when --only is provided with the group' do
      let(:only) { ['group1'] }

      it_behaves_like 'generates and writes policy files'
    end

    context 'when --only is provided with another group' do
      let(:only) { ['source code'] }

      it 'does not generate anything' do
        expect(File)
          .not_to receive(:write)

        described_class.run(options)
      end
    end
  end
end
