# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/label_jihu_contribution'
require_relative '../../triage/triage/event'

RSpec.describe Triage::LabelJiHuContribution do
  include_context 'with event', Triage::MergeRequestNoteEvent do
    let(:event_attrs) do
      {
        action: 'open',
        jihu_contributor?: false
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open']

  describe '#applicable?' do
    context 'when event is for a new merge request opened by a JiHu contributor' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when event is not from a JiHu contributor' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a message to apply the label' do
      body = <<~MARKDOWN.chomp
        /label ~"JiHu contribution"
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
