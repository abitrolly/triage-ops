# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/thank_contribution'

RSpec.describe Triage::ThankContribution do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        event_actor_username: 'root',
        object_kind: 'merge_request'
      }
    end
  end

  include_context 'with merge request notes'

  let(:from_community_fork) { true }
  let(:first_contribution) { false }

  before do
    allow(event).to receive(:from_community_fork?).and_return(from_community_fork)
    allow(event).to receive(:first_contribution?).and_return(first_contribution)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open']

  describe '#applicable?' do
    let(:wider_community_author) { true }
    let(:from_gitlab_group) { true }
    let(:project_path_with_namespace) { "" }

    before do
      allow(event).to receive(:wider_community_author?).and_return(wider_community_author)
      allow(event).to receive(:from_gitlab_group?).and_return(from_gitlab_group)
      allow(event).to receive(:project_path_with_namespace).and_return(project_path_with_namespace)
    end

    it_behaves_like 'event is applicable'

    context 'when event author is not from the wider community' do
      let(:wider_community_author) { false }

      include_examples 'event is not applicable'
    end

    context 'when event is not from a gitlab group' do
      let(:from_gitlab_group) { false }

      include_examples 'event is not applicable'
    end

    context 'when for project with custom conditions' do
      let(:project_id) { described_class::WWW_GITLAB_COM_PROJECT_ID }

      it 'processes different conditions' do
        expect(event).to receive(:wider_community_author?).and_return(true)

        expect(subject).to be_applicable
      end
    end

    context 'when for project with excluded path prefix' do
      let(:project_path_with_namespace) { "gitlab-org/incubation-engineering/cloud-seed-trusted-testers/jean-philippe-baconnais/demo-cloud-seed" }

      include_examples 'event is not applicable'
    end

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { body: 'review comment 1' },
          { body: comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    before do
      allow(Triage).to receive(:api_client).and_call_original
      allow(Triage).to receive_message_chain(:api_client, :update_merge_request)
    end

    let(:expected_message) do
      add_automation_suffix do
        <<~MARKDOWN.chomp
          #{comment_mark}
          Thanks for your contribution to GitLab @root! :tada:

          * If you need help, comment @gitlab-bot help or come say hi on [Discord](https://discord.gg/gitlab).
          * When you're ready for a review, comment on this merge request with @gitlab-bot ready.
          * We welcome AI-generated contributions! Read more/check the box at the top of the merge request description.
          * To [add labels to your merge request](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations/#reactive-label-and-unlabel-commands), comment @gitlab-bot label ~"label1" ~"label2".

          /label ~"Community contribution" ~"workflow::in dev"
          /assign @root
        MARKDOWN
      end
    end

    it 'posts a default message' do
      expect_comment_request(event: event, body: expected_message) do
        subject.process
      end
    end

    it 'adds the AI contribution checkbox to the description' do
      expect_comment_request(event: event, body: expected_message) do
        subject.process
      end

      expect(Triage.api_client).to have_received(:update_merge_request)
    end

    context 'when not from a community fork' do
      let(:expected_message) do
        <<~MARKDOWN.chomp
          Thanks for your contribution to GitLab @root! :tada:

          Did you know about our [community forks](https://gitlab.com/gitlab-community/meta)?
          Working from there will make your contribution process easier. Please check it out!
        MARKDOWN
      end

      let(:from_community_fork) { false }

      it 'includes a note about the community forks' do
        expect_comment_request(event: event, body: include(expected_message)) do
          subject.process
        end
      end
    end

    context 'when a first contribution' do
      let(:expected_message) do
        add_automation_suffix do
          <<~MARKDOWN.chomp
            #{comment_mark}
            Thanks for your contribution to GitLab @root! :tada:

            * If you need help, comment @gitlab-bot help or come say hi on [Discord](https://discord.gg/gitlab).
            * When you're ready for a review, comment on this merge request with @gitlab-bot ready.
            * We welcome AI-generated contributions! Read more/check the box at the top of the merge request description.


            /label ~"Community contribution" ~"workflow::in dev"
            /assign @root
            /label ~"1st contribution"
          MARKDOWN
        end
      end

      let(:unexpected_message) do
        <<~MARKDOWN.chomp
        * To [add labels to your merge request](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations/#reactive-label-and-unlabel-commands), comment @gitlab-bot label ~"label1" ~"label2".
        MARKDOWN
      end

      let(:first_contribution) { true }

      it "adds a thank you and doesn't mention labels" do
        expect_comment_request(event: event, body: expected_message) do
          subject.process
        end
      end
    end

    context 'when GitLab FOSS' do
      let(:project_id) { 13_083 }
      let(:expected_message) do
        add_automation_suffix do
          <<~MARKDOWN.chomp
            #{comment_mark}
            Hey @root! :wave:

            Thank you for your contribution. GitLab has moved to a single codebase for GitLab CE and GitLab EE.

            Please do not create merge requests here. Instead, create them at https://gitlab.com/gitlab-org/gitlab/-/merge_requests.

            /close

            /label ~"Community contribution" ~"workflow::in dev"
            /assign @root
          MARKDOWN
        end
      end

      it 'posts custom project comment' do
        expect_comment_request(event: event, body: expected_message) do
          subject.process
        end
      end

      it 'does not add the AI contribution checkbox' do
        expect_comment_request(event: event, body: expected_message) do
          subject.process
        end

        expect(Triage.api_client).not_to have_received(:update_merge_request)
      end
    end

    context 'when runner' do
      let(:project_id) { 250_833 }
      let(:expected_message) do
        add_automation_suffix do
          <<~MARKDOWN.chomp
            #{comment_mark}
            Thanks for your contribution to GitLab @root! :tada:

            * If you need help, comment @gitlab-bot help or come say hi on [Discord](https://discord.gg/gitlab).
            * When you're ready for a review, comment on this merge request with @gitlab-bot ready.
            * We welcome AI-generated contributions! Read more/check the box at the top of the merge request description.

            Some contributions require several iterations of review and we try to mentor contributors
            during this process. However, we understand that some reviews can be very time consuming.
            If you would prefer for us to continue the work you've submitted now or at any point in the
            future please let us know.

            If you're okay with being part of our review process (and we hope you are!), there are
            several initial checks we ask you to make:

            * The merge request description clearly explains:
              * The problem being solved.
              * The best way a reviewer can test your changes (is it possible to provide an example?).
            * If the pipeline failed, do you need help identifying what failed?
            * Check that Go code follows our [Go guidelines](https://docs.gitlab.com/ee/development/go_guide/index.html#code-review).
            * Read our [contributing to GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/CONTRIBUTING.md#contribute-to-gitlab-runner)
            document.

            /label ~"Community contribution" ~"workflow::in dev"
            /assign @root
          MARKDOWN
        end
      end

      it 'posts custom project comment' do
        expect_comment_request(event: event, body: expected_message) do
          subject.process
        end
      end
    end

    context 'when www-gitlab-com' do
      let(:project_id) { 7764 }
      let(:expected_message) do
        add_automation_suffix do
          <<~MARKDOWN.chomp
            #{comment_mark}
            Thanks for your contribution to GitLab @root! :tada:

            * When you're ready for a review, comment on this merge request with @gitlab-bot ready.
            * We welcome AI-generated contributions! Read more/check the box at the top of the merge request description.

            I'll notify the website team about your merge request and they will get back to you as soon
            as they can.
            If you don't hear from someone in a reasonable amount of time, please ping us again in a
            comment and mention @gitlab-com-community.

            /label ~"Community contribution" ~"workflow::in dev"
            /assign @root
          MARKDOWN
        end
      end

      it 'posts custom project comment' do
        expect_comment_request(event: event, body: expected_message) do
          subject.process
        end
      end
    end
  end
end
