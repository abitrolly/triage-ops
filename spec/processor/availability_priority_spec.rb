# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/availability_priority'
require_relative '../../triage/triage/event'

RSpec.describe Triage::AvailabilityPriority do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        action: 'update',
        from_gitlab_org?: true
      }
    end

    let(:label_names) { ['bug::availability'] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.open", "issue.reopen", "issue.update", "merge_request.update", "merge_request.open", "merge_request.reopen"]

  describe '#applicable?' do
    context 'when all conditions are met' do
      include_examples 'event is applicable'
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when resource is not opened' do
      before do
        allow(event).to receive(:resource_open?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when there is no availability label' do
      let(:label_names) { [] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    context 'when there is availability label' do
      it 'calls process_availability_priority' do
        expect(subject).to receive(:process_availability_priority)

        subject.process
      end

      context 'when there is also severity label' do
        shared_examples 'default priority' do |priority|
          context 'when there is no priority label' do
            it "adds #{priority}" do
              body = <<~MARKDOWN.chomp
                /label ~"#{priority}"
              MARKDOWN

              expect_comment_request(event: event, body: body) do
                subject.process
              end
            end
          end
        end

        shared_examples 'doing nothing' do |accept:|
          accept.each do |priority|
            context "when there is also #{priority} label" do
              before do
                label_names << priority
              end

              it 'does nothing' do
                expect(subject.process).to be_nil
              end
            end
          end
        end

        shared_examples 'swapping to default priority' do |accept:|
          default = accept.first

          (%w[priority::1 priority::2 priority::3 priority::4] - accept).each do |priority|
            context "when there is also #{priority}" do
              before do
                label_names << priority
              end

              it "adds #{priority} and remove other priority labels" do
                accept_list = accept.map { |p| "~#{p}" }.join(', or ')

                body = add_automation_suffix do
                  <<~MARKDOWN.chomp
                    ~"#{severity}" with ~"#{described_class::AVAILABILITY_LABEL}" can only have #{accept_list}

                    Please see the [Availability prioritization guidelines](https://about.gitlab.com/handbook/engineering/performance/#availability) for more detail.

                    /unlabel ~"#{priority}"
                    /label ~"#{default}"
                  MARKDOWN
                end

                expect_comment_request(event: event, body: body) do
                  subject.process
                end
              end
            end
          end
        end

        before do
          label_names << severity
        end

        context 'when there is also severity::1 label' do
          let(:severity) { 'severity::1' }

          it_behaves_like 'default priority', 'priority::1'
          it_behaves_like 'doing nothing', accept: %w[priority::1]
          it_behaves_like 'swapping to default priority', accept: %w[priority::1]
        end

        context 'when there is also severity::2 label' do
          let(:severity) { 'severity::2' }

          it_behaves_like 'default priority', 'priority::1'
          it_behaves_like 'doing nothing', accept: %w[priority::1]
          it_behaves_like 'swapping to default priority', accept: %w[priority::1]
        end

        context 'when there is also severity::3 label' do
          let(:severity) { 'severity::3' }

          it_behaves_like 'default priority', 'priority::2'
          it_behaves_like 'doing nothing', accept: %w[priority::2 priority::1]
          it_behaves_like 'swapping to default priority', accept: %w[priority::2 priority::1]
        end

        context 'when there is also severity::4 label' do
          let(:severity) { 'severity::4' }

          it_behaves_like 'default priority', 'priority::3'
          it_behaves_like 'doing nothing', accept: %w[priority::3 priority::2 priority::1]
          it_behaves_like 'swapping to default priority', accept: %w[priority::3 priority::2 priority::1]
        end
      end
    end
  end
end
