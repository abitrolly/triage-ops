# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/engineering_productivity/remind_merged_mr_deviating_from_guidelines'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::RemindMergedMrDeviatingFromGuideline do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        from_gitlab_org_gitlab?: true,
        target_branch_is_main_or_master?: true,
        project_id: project_id,
        iid: iid
      }
    end

    let(:project_id) { 123 }
    let(:iid) { 456 }
    let(:latest_pipeline_id) { 42 }
    let(:latest_pipeline_ref) { "refs/merge-requests/#{iid}/merge" }
    let(:latest_pipeline_project_id) { project_id }
    let(:latest_pipeline_created_at) { Time.now - (3600 * (described_class::HOURS_THRESHOLD_FOR_CREATED_STALE_MR_PIPELINE + 1)) }
    let(:latest_pipeline_non_detailed) do
      {
        id: latest_pipeline_id,
        ref: latest_pipeline_ref,
        project_id: latest_pipeline_project_id
      }
    end

    let(:latest_pipeline_detailed) do
      latest_pipeline_non_detailed.merge(
        created_at: latest_pipeline_created_at
      )
    end

    let(:pipelines) do
      [
        latest_pipeline_non_detailed,
        {
          id: 12
        }
      ]
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.merge']

  describe '#applicable?' do
    before do
      allow(event).to receive(:with_project_id?).and_return(true)
    end

    context 'when event project is not under gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'with API call for pipelines' do
      before do
        stub_api_request(path: "/projects/#{project_id}/merge_requests/#{iid}/pipelines", response_body: pipelines)
        stub_api_request(path: "/projects/#{project_id}/pipelines/#{latest_pipeline_id}", response_body: latest_pipeline_detailed)
      end

      include_examples 'event is applicable'

      context 'when MR does not target the main branch' do
        before do
          allow(event).to receive(:target_branch_is_main_or_master?).and_return(false)
        end

        include_examples 'event is not applicable'
      end

      context 'when latest pipeline not from canonical project' do
        let(:project_id) { 999 }

        before do
          # Ideally we'd use a partial double for event so that we don't have to stub this method
          allow(event).to receive(:with_project_id?).with(project_id).and_return(false)
        end

        include_examples 'event is applicable'
      end

      context 'when latest pipeline from canonical project' do
        context 'when latest pipeline is nil' do
          let(:pipelines) { [] }

          include_examples 'event is applicable'
        end

        context 'when latest pipeline recent enough' do
          let(:latest_pipeline_created_at) { Time.now }

          include_examples 'event is not applicable'
        end

        context 'when latest pipeline ref is not merged results' do
          let(:latest_pipeline_ref) { "refs/merge-requests/#{iid}/head" }

          include_examples 'event is applicable'
        end

        context 'when latest pipeline ref is a merge train' do
          let(:latest_pipeline_ref) { "refs/merge-requests/#{iid}/train" }

          include_examples 'event is not applicable'
        end
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'calls remind_merged_mr_deviating_from_guidelines' do
      expect(subject).to receive(:remind_merged_mr_deviating_from_guidelines)

      subject.process
    end

    it 'posts a comment' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          @#{event.event_actor_username}, did you forget to run a pipeline before you merged this work?

          Based on our [code review process](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request),
          the latest pipeline before merge must match the following conditions:

          1. Pipeline is a canonical one (i.e. didn't run in the context of a fork)
          2. Pipeline is a ["tier-3"](https://docs.gitlab.com/ee/development/pipelines/#pipeline-tiers) one (i.e. the merge request has the ~"pipeline::tier-3" label set)
          3. Pipeline is a [Merged Results](https://docs.gitlab.com/ee/ci/pipelines/merged_results_pipelines.html) one
          4. Pipeline was created at most 8 hours ago

          This rule shouldn't be ignored as this can lead to a broken `master`. Please consider replying to this comment for transparency.
          /label ~"pipeline:too-old"
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
