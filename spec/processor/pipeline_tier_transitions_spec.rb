# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/pipeline_tier_transitions'
require_relative '../../triage/triage/event'

RSpec.describe Triage::PipelineTierTransitions do
  include_context 'with event', Triage::MergeRequestEvent do
    # TODO: When removing the sandbox, please use the following project instead
    # let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
    let(:project_id) { Triage::Event::TRIAGE_OPS_PLAYGROUND_PROJECT_ID }
    let(:merge_request_iid) { 300 }

    let(:approved) { false }
    let(:approvals_required) { 2 }
    let(:approvals_left) { 2 }
    let(:approvers) { [] }

    let(:approvals) do
      Gitlab::ObjectifiedHash.new(
        approved: approved,
        approvals_required: approvals_required,
        approvals_left: approvals_left
      )
    end

    let(:label_names)        { ['bug::availability'] }
    let(:team_member_author) { true }
    let(:automation_author)  { false }

    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approved',
        gitlab_bot_event_actor?: false,
        from_gitlab_org_gitlab?: true,
        team_member_author?: team_member_author,
        automation_author?: automation_author,
        jihu_contributor?: false,
        source_branch_is?: false,
        target_branch_is_stable_branch?: false,
        approvals: approvals,
        approvers: approvers,
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  let(:instance) { described_class.new(event) }
  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "lib/gitlab.rb",
          "new_path" => "lib/gitlab.rb"
        }
      ]
    }
  end

  subject { instance }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  include_examples 'registers listeners', [
    'merge_request.open',
    'merge_request.approval',
    'merge_request.approved',
    'merge_request.unapproval',
    'merge_request.unapproved',
    'merge_request.update'
  ]

  describe '#applicable?' do
    # TODO: When going away from sandbox to gitlab-org/gitlab
    #
    #   * Remove the block below
    #   * uncomment the following blocks
    context 'when event project is coming from the triage-ops sandbox' do
      let(:project_id) { Triage::Event::TRIAGE_OPS_PLAYGROUND_PROJECT_ID }

      include_examples 'event is applicable'
    end

    # context 'when event project is not under gitlab-org/gitlab' do
    #   before do
    #     allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
    #   end

    #   include_examples 'event is not applicable'
    # end
    #
    # context 'when event project is under gitlab-org/gitlab' do
    #   before do
    #     allow(event).to receive(:from_gitlab_org_gitlab?).and_return(true)
    #   end

    #   include_examples 'event is applicable'
    # end
    #
    # context 'when event project is under gitlab-org/security/gitlab' do
    #   before do
    #     allow(event).to receive(:from_gitlab_org_security_gitlab?)
    #       .and_return(true)
    #   end
    #
    #   include_examples 'event is applicable'
    # end

    context 'when the event is coming from a bot' do
      before do
        allow(event).to receive(:gitlab_bot_event_actor?)
          .and_return(true)
      end

      include_examples 'event is not applicable'
    end

    describe 'need_pipeline_tier_label?' do
      before do
        fake_instance = instance_double(Triage::NeedMrApprovedLabel)
        allow(Triage::NeedMrApprovedLabel).to receive(:new).and_return(fake_instance)
        allow(fake_instance).to receive(:need_mr_approved_label?).and_return(need_pipeline_tier_label_value)
      end

      context 'when need_pipeline_tier_label? is true' do
        let(:need_pipeline_tier_label_value) { true }

        include_examples 'event is applicable'
      end

      context 'when need_mr_approved_label? is false' do
        let(:need_pipeline_tier_label_value) { false }

        include_examples 'event is not applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    describe 'adding ~pipeline::tier-X labels' do
      let(:approved)           { false }
      let(:approvals_required) { 2 }
      let(:approvals_left)     { 2 }
      let(:approvers)          { [] }

      before do
        stub_api_request(
          path: "/projects/12345/issues/6789/notes",
          query: { per_page: 100 },
          response_body: []
        )
      end

      context 'when the MR was not approved by anybody' do
        let(:approvers) { [] }

        it 'adds the pipeline::tier-1 label' do
          body = <<~MARKDOWN.chomp
            /label ~\"#{Labels::PIPELINE_TIER_1_LABEL}\"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end

        context 'when the Labels::MR_APPROVED_LABEL label is not present' do
          let(:label_names) { [] }

          it 'does not add the Labels::MR_APPROVED_LABEL label' do
            body = <<~MARKDOWN.chomp
              /label ~\"#{Labels::PIPELINE_TIER_1_LABEL}\"
            MARKDOWN

            expect_comment_request(event: event, body: body) do
              subject.process
            end
          end
        end

        context 'when the tier label is already present' do
          let(:label_names) { [Labels::PIPELINE_TIER_1_LABEL] }

          it 'does not add the same tier label again' do
            expect(subject).not_to receive(:add_comment)

            subject.process
          end
        end
      end

      context 'when the MR was approved, but still needs some approvals' do
        let(:approved)       { true }
        let(:approvals_left) { 1 }
        let(:approvers)      { [{ 'id' => 567, 'username' => 'julie' }] }
        let(:label_names)    { [Labels::MR_APPROVED_LABEL] } # we assume this label was already present by default

        it 'adds the pipeline::tier-2 label' do
          body = <<~MARKDOWN.chomp
            /label ~\"#{Labels::PIPELINE_TIER_2_LABEL}\"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end

        context 'when the Labels::MR_APPROVED_LABEL label is not present' do
          let(:label_names) { [] }

          it 'also adds the Labels::MR_APPROVED_LABEL label' do
            body = <<~MARKDOWN.chomp
              /label ~\"#{Labels::PIPELINE_TIER_2_LABEL}\" ~\"#{Labels::MR_APPROVED_LABEL}\"
            MARKDOWN

            expect_comment_request(event: event, body: body) do
              subject.process
            end
          end
        end

        context 'when the tier label is already present' do
          before do
            label_names << Labels::PIPELINE_TIER_2_LABEL
          end

          it 'does not add the same tier label again' do
            expect(subject).not_to receive(:add_comment)

            subject.process
          end
        end
      end

      context 'when the MR has all the approvals it needs' do
        let(:approved)       { true }
        let(:approvals_left) { 0 }
        let(:approvers) do
          [
            { 'id' => 567, 'username' => 'julie' },
            { 'id' => 123, 'username' => 'bob' }
          ]
        end
        let(:label_names) { [Labels::MR_APPROVED_LABEL] } # we assume this label was already present by default

        it 'adds the pipeline::tier-3 label' do
          body = <<~MARKDOWN.chomp
            /label ~\"#{Labels::PIPELINE_TIER_3_LABEL}\"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end

        context 'when the Labels::MR_APPROVED_LABEL label is not present' do
          let(:label_names) { [] }

          it 'also adds the Labels::MR_APPROVED_LABEL label' do
            body = <<~MARKDOWN.chomp
              /label ~\"#{Labels::PIPELINE_TIER_3_LABEL}\" ~\"#{Labels::MR_APPROVED_LABEL}\"
            MARKDOWN

            expect_comment_request(event: event, body: body) do
              subject.process
            end
          end
        end

        context 'when the tier label is already present' do
          before do
            label_names << Labels::PIPELINE_TIER_3_LABEL
          end

          it 'does not add the same tier label again' do
            expect(subject).not_to receive(:add_comment)

            subject.process
          end
        end
      end
    end

    describe 'triggering a new pipeline' do
      # Going from tier 1 to tier 2
      let(:approvals_left) { 1 }
      let(:approvers)      { [{ 'id' => 567, 'username' => 'julie' }] }
      let(:label_names)    { [Labels::PIPELINE_TIER_1_LABEL] }

      # There is a previous discussion by default
      let(:previous_discussion) { double('discussion') } # rubocop:disable RSpec/VerifiedDoubles -- API of an external gem

      # We isolate the pipeline trigger for this block.
      before do
        allow(subject).to receive(:add_comment)
        allow(instance).to receive_message_chain(:unique_comment, :previous_discussion).and_return(previous_discussion)
      end

      context 'when the author is a team member' do
        let(:team_member_author) { true }
        let(:automation_author)  { false }

        it 'triggers a new pipeline' do
          expect(Triage::TriggerPipelineOnApprovalJob).to receive(:perform_in)

          subject.process
        end
      end

      context 'when the author is an authorized automation' do
        let(:team_member_author) { false }
        let(:automation_author)  { true }

        it 'triggers a new pipeline' do
          expect(Triage::TriggerPipelineOnApprovalJob).to receive(:perform_in)

          subject.process
        end
      end

      context 'when the author is not a team member, nor an authorized automation' do
        let(:team_member_author) { false }
        let(:automation_author)  { false }

        it 'does not trigger a new pipeline' do
          expect(Triage::TriggerPipelineOnApprovalJob).not_to receive(:perform_in)

          subject.process
        end
      end
    end

    describe 'creating the pipeline tier discussion' do
      # Going from tier 1 to tier 2
      let(:approvals_left)      { 1 }
      let(:approvers)           { [{ 'id' => 567, 'username' => 'julie' }] }
      let(:label_names)         { [Labels::PIPELINE_TIER_1_LABEL] }

      # There is a previous discussion by default
      let(:previous_discussion) { double('discussion') } # rubocop:disable RSpec/VerifiedDoubles -- API of an external gem

      # We isolate the pipeline tier discussion creation for this block.
      before do
        allow(subject).to receive(:add_comment)
        allow(Triage::TriggerPipelineOnApprovalJob).to receive(:perform_in)
        allow(instance).to receive_message_chain(:unique_comment, :previous_discussion).and_return(previous_discussion)
      end

      context 'when the previous discussion is absent' do
        let(:previous_discussion) { nil }

        context 'when a new pipeline will be triggered automatically' do
          let(:team_member_author) { true }
          let(:automation_author)  { false }

          let(:body) do
            <<~MARKDOWN.strip
              ## Before you set this MR to auto-merge

              This merge request will progress on pipeline tiers until it reaches the last tier: ~"pipeline::tier-3".
              We will trigger a new pipeline for each transition to a higher tier.

              **Before you resolve this discussion**, please check the following:

              * You are **the last maintainer** of this merge request
              * The latest pipeline for this merge request is ~"pipeline::tier-3" (You can find which tier it is in the pipeline name)
              * This pipeline is recent enough (**created in the last 4 hours**)

              If all the criteria above apply, please resolve this discussion and the set auto-merge for this merge request.

              See [pipeline tiers](https://docs.gitlab.com/ee/development/pipelines/#pipeline-tiers) and [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request) for more details.
            MARKDOWN
          end

          it 'creates the pipeline tier discussion' do
            allow(instance).to receive_message_chain(:unique_comment, :wrap).with(body).and_return('our stubbed message')

            expect_discussion_request(event: event, body: 'our stubbed message') do
              subject.process
            end
          end
        end

        context 'when a new pipeline will not be triggered automatically' do
          let(:team_member_author) { false }
          let(:automation_author)  { false }

          let(:body) do
            <<~MARKDOWN.strip
              ## Before you set this MR to auto-merge

              This merge request will progress on pipeline tiers until it reaches the last tier: ~"pipeline::tier-3".


              **Before you resolve this discussion**, please check the following:

              * You are **the last maintainer** of this merge request
              * The latest pipeline for this merge request is ~"pipeline::tier-3" (You can find which tier it is in the pipeline name)
              * This pipeline is recent enough (**created in the last 4 hours**)

              If all the criteria above apply, please resolve this discussion and the set auto-merge for this merge request.

              See [pipeline tiers](https://docs.gitlab.com/ee/development/pipelines/#pipeline-tiers) and [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request) for more details.
            MARKDOWN
          end

          it 'creates the pipeline tier discussion' do
            allow(instance).to receive_message_chain(:unique_comment, :wrap).with(body).and_return('our stubbed message')

            expect_discussion_request(event: event, body: 'our stubbed message') do
              subject.process
            end
          end
        end
      end

      context 'when the previous discussion is already present' do
        it 'does not create the pipeline tier discussion' do
          expect_no_request { subject.process }
        end
      end
    end
  end
end
