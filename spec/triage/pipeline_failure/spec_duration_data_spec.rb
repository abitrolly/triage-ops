# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/pipeline_failure/spec_duration_data'

RSpec.describe Triage::PipelineFailure::SpecDurationData do
  let(:spec_file) { 'spec1.rb' }
  let(:expected_duration) { '101.1' }
  let(:actual_duration) { '201.1' }

  subject do
    described_class.new(
      spec_file: spec_file,
      expected_duration: expected_duration,
      actual_duration: actual_duration
    ).markdown_table_row
  end

  describe '#markdown_table_row' do
    it { is_expected.to eq '|spec1.rb|1 minute 41.1 seconds|3 minutes 21.1 seconds|+98%|' }

    context 'when expected duration is nil' do
      let(:expected_duration) { nil }

      it { is_expected.to eq '|spec1.rb||3 minutes 21.1 seconds||' }
    end

    context 'when actual duration is nil' do
      let(:actual_duration) { nil }

      it { is_expected.to eq '|spec1.rb|1 minute 41.1 seconds|||' }
    end

    context 'when expected and actual duration are less than 1 seconds' do
      let(:actual_duration) { '0.2' }
      let(:expected_duration) { '0.1' }

      it { is_expected.to eq '|spec1.rb|0.1 second|0.2 second|+100%|' }
    end
  end
end
