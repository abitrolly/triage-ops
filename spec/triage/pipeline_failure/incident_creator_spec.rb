# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/incident_creator'
require_relative '../../../triage/triage/pipeline_failure/config/master_branch'
require_relative '../../../triage/triage/pipeline_failure/config/dev_master_branch'
require_relative '../../../triage/triage/pipeline_failure/config/stable_branch'

RSpec.describe Triage::PipelineFailure::IncidentCreator do
  let(:ref) { 'master' }
  let(:event) do
    instance_double(Triage::PipelineEvent,
      id: 42,
      project_path_with_namespace: 'gitlab-org/gitlab',
      project_id: 999,
      ref: ref,
      sha: 'sha',
      source_job_id: nil,
      web_url: 'web_url',
      commit_header: 'commit_header',
      event_actor: Triage::User.new(id: '1', name: 'John Doe', username: 'john'),
      source: 'source',
      created_at: Time.now,
      merge_request: Triage::MergeRequest.new(title: 'Hello', author: { id: '1' }),
      merge_request_pipeline?: false,
      project_web_url: "https://gitlab.test/gitlab-org/gitlab",
      instance: :com)
  end

  let(:first_job_name) { 'foo' }
  let(:second_job_name) { 'bar' }
  let(:failed_jobs) do
    [
      double(id: '1', name: first_job_name, web_url: 'foo_web_url'),
      double(id: '2', name: second_job_name, web_url: 'foo_web_url')
    ]
  end

  let(:incident_project_id) { config.incident_project_id }
  let(:incident_iid)        { 4567 }

  let(:incident_double)     { double('Incident', project_id: incident_project_id, iid: incident_iid) }

  let(:top_root_cause_label) { 'master-broken::undetermined' }
  let(:top_group_label) { nil }
  let(:root_cause_analysis_comment) { nil }
  let(:investigation_comment) { nil }
  let(:duplicate_incident_url) { nil }
  let(:test_level_labels) { ['test-level:unit', 'test-level:integration'] }
  let(:triager) do
    instance_double(Triage::PipelineFailure::TriageIncident,
      attribution_comment: '',
      top_root_cause_label: top_root_cause_label,
      top_group_label: top_group_label,
      test_level_labels: test_level_labels,
      root_cause_analysis_comment: root_cause_analysis_comment,
      investigation_comment: investigation_comment,
      duration_analysis_comment: nil,
      duplicate_incident_url: duplicate_incident_url)
  end

  subject(:incident_creator) { described_class.new(event: event, config: config, failed_jobs: failed_jobs) }

  around do |example|
    Timecop.freeze(Time.utc(2020, 3, 31, 8)) { example.run }
  end

  before do
    allow(Triage::PipelineFailure::TriageIncident).to receive(:new).and_return(triager)
    allow(Triage.api_client).to receive(:issue).with(incident_project_id, incident_iid)
  end

  describe '#execute' do
    shared_examples 'incident creation' do
      let(:discussion_path) { "/projects/#{incident_project_id}/issues/#{incident_double.iid}/discussions" }
      let(:job_trace) { 'ERROR: Job failed: failed to pull image "registry.gitlab.com"' }

      context 'when ref is master' do
        let(:config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }

        context 'with job trace showing RSpec test failures' do
          let(:root_cause_analysis_comment) { "\n\nroot_cause_analysis_comment" }
          let(:investigation_comment) { "\n\ninvestigation_comment" }

          it 'creates an incident, attributes to a group, and posts relevant job trace to investigation steps' do
            expect(Triage.api_client).to receive(:create_issue)
              .with(
                incident_project_id,
                expected_incident_title,
                {
                  issue_type: 'incident',
                  description: format(config.incident_template, incident_creator.__send__(:template_variables)),
                  labels: ['Engineering Productivity', 'master:broken', 'master-broken::undetermined', *test_level_labels]
                })
              .and_return(incident_double)

            expect(Triage.api_client).to receive(:post)
              .with(
                discussion_path,
                body: {
                  body: <<~MARKDOWN.chomp
                    ## Root Cause Analysis

                    root_cause_analysis_comment
                  MARKDOWN
                }
              )

            expect(Triage.api_client).to receive(:post)
              .with(
                discussion_path,
                body: {
                  body: <<~MARKDOWN.chomp
                    ## Investigation Steps

                    investigation_comment
                  MARKDOWN
                })

            expect(Triage.api_client).to receive(:post)
              .with(
                discussion_path,
                body: { body: '## Duration Analysis' })

            incident_creator.execute
          end
        end
      end
    end

    it_behaves_like 'incident creation' do
      let(:expected_incident_title) { "Tuesday 2020-03-31 08:00 UTC - `gitlab-org/gitlab` broken `#{ref}` with #{first_job_name}, #{second_job_name}" }
    end

    context 'with a long job name' do
      let(:second_job_name) { 'a' * 180 }

      it_behaves_like 'incident creation' do
        let(:expected_incident_title) { "Tuesday 2020-03-31 08:00 UTC - `gitlab-org/gitlab` broken `#{ref}` with #{first_job_name}, #{'a' * 175}..." }
      end
    end

    context 'when the previous incident failed the same jobs and was closed as a duplicate of an earlier incident' do
      let(:config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }
      let(:root_cause_analysis_comment) { "\n\nroot_cause_analysis_comment" }
      let(:duplicate_incident_url) { "https://gitlab.example.com/api/v4/projects/1/issues/1" }
      let(:discussion_path) { "/projects/#{incident_project_id}/issues/#{incident_double.iid}/discussions" }
      let(:expected_incident_title) { "Tuesday 2020-03-31 08:00 UTC - `gitlab-org/gitlab` broken `#{ref}` with foo, bar" }

      it 'creates an incident and marks as duplicate' do
        expect(Triage.api_client).to receive(:create_issue)
          .with(
            incident_project_id,
            expected_incident_title,
            {
              issue_type: 'incident',
              description: format(config.incident_template, incident_creator.__send__(:template_variables)),
              labels: [*config.incident_labels, 'master-broken::undetermined', *test_level_labels]
            })
          .and_return(incident_double)

        expect(Triage.api_client).to receive(:post)
          .with(
            discussion_path,
            body: {
              body: <<~MARKDOWN.chomp
                ## Root Cause Analysis

                root_cause_analysis_comment
              MARKDOWN
            }
          )

        expect(Triage.api_client).to receive(:post)
          .with(
            discussion_path,
            body: {
              body: <<~MARKDOWN.chomp
              ## Investigation Steps
              MARKDOWN
            }
          )

        expect(Triage.api_client).to receive(:post)
          .with(
            discussion_path,
            body: { body: '## Duration Analysis' })

        incident_creator.execute
      end
    end
  end
end
