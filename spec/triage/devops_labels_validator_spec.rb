# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/triage/devops_labels_validator'
require_relative '../../triage/resources/issue'

RSpec.describe Triage::DevopsLabelsValidator do
  let(:label_names) { [] }

  subject { described_class.new(label_names) }

  describe 'labels_set?' do
    context 'with no label' do
      let(:label_names) { [] }

      it 'returns false' do
        expect(subject.labels_set?).to be false
      end
    end

    context 'with section label' do
      let(:label_names) { ['section::growth'] }

      it 'returns false' do
        expect(subject.labels_set?).to be false
      end
    end

    context 'with group label' do
      let(:label_names) { ['group::group1'] }

      it 'returns true' do
        expect(subject.labels_set?).to be true
      end
    end

    context 'with category label' do
      let(:label_names) { ['Category:Groups & Projects'] }

      it 'returns true' do
        expect(subject.labels_set?).to be true
      end
    end

    context 'with special team label' do
      let(:label_names) { ['meta'] }

      it 'returns true' do
        expect(subject.labels_set?).to be true
      end
    end
  end
end
