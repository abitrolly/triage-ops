# Running triage-ops locally

[[_TOC_]]

## triage-ops pry console

```shell
bundle exec rack-console
```

From there, you'll be able to try some parts of triage-ops in the console :tada:

```ruby
# Load an event payload from the spec fixtures
payload = JSON.parse(File.read('spec/fixtures/reactive/gitlab_test_note.json'))

event = Triage::Event.build(payload)
=> #<Triage::NoteEvent:0x0000000126a5c9c8
 @payload={"object_kind"=>"note", "project"=>{"id"=>1, "path_with_namespace"=>"gitlab-org/gitlab-test"}, "object_attributes"=>{"note"=>"@gitlab-bot label ~\"group::source code\"", "noteable_type"=>"Issue"}, "issue"=>{"iid"=>1000000000000}}>

[3] pry(main)> event.note?
=> true
```

## Setup local GitLab instance

`triage-ops` service listens and reacts to events from a GitLab instance (e.g. GDK).

To setup triage data in local GitLab instance, run this command in the GitLab Rails project directory:

```shell
cd <local-gitlab-instance-folder>
bundle exec rake db:seed_fu FILTER=33_triage_ops SEED_TRIAGE_OPS=true
```

This command uses [Triage Ops seed file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/fixtures/development/33_triage_ops.rb) that:

- Creates necessary groups and projects
- Configures to send webhook events to local endpoint
- Creates and prints a new `GITLAB_BOT_API_TOKEN_ON_COM` and `GITLAB_WEBHOOK_TOKEN`.

### Troubleshooting Triage Ops seed failure

If you fail to seed `33_triage_ops` due to the following error:

```shell
OpenSSL::Cipher::CipherError:
~/src/gitlab-development-kit/gitlab/vendor/gems/attr_encrypted/lib/attr_encrypted.rb:242:in `attr_decrypt'
```

This could mean that the encryption key has changed, and you will need to run `gdk reset-data` to reset your local data.

## Run triage-ops service

```shell
GITLAB_API_ENDPOINT=http://127.0.0.1:3000 \
  GITLAB_WEBHOOK_TOKEN=triage-ops-webhook-token \
  GITLAB_BOT_API_TOKEN_ON_COM=<API_TOKEN_PRINTED_IN_RAKE_TASK_ABOVE> \
  SLACK_WEBHOOK_URL=https://example.org \
  DRY_RUN=1 \
  bundle exec puma -b tcp://0.0.0.0:8091
```

NOTE:
- `DRY_RUN=1` allows you to test triage without making API calls to GitLab.
- Remove or set `DRY_RUN=0` if/when you want API calls to be made.

You can now test the endpoint:

```shell
curl -X POST 0.0.0.0:8091 -H "Content-Type: application/json" -H "X-Gitlab-Token: triage-ops-webhook-token" -d @spec/fixtures/reactive/gitlab_test_note.json
```

## Running service in container

```shell
# Docker command
#
# Note: You probably should not use docker desktop at GitLab.
# See https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop for more info
#
# Also, host.docker.internal is for macOS hosts only. For linux, you might want to use 172.17.0.1
#
# (source: https://stackoverflow.com/questions/48546124/what-is-linux-equivalent-of-host-docker-internal)
{
  docker rm `docker ps -aq`
  docker build --file=Dockerfile.rack -t triage-ops .
  docker run -itp 8091:8080 \
    -e GITLAB_API_ENDPOINT=http://host.docker.internal:3000/api/v4 \
    -e GITLAB_WEBHOOK_TOKEN=triage-ops-webhook-token \
    -e GITLAB_BOT_API_TOKEN_ON_COM=<API_TOKEN_PRINTED_IN_RAKE_TASK_ABOVE> \
    -e SLACK_WEBHOOK_URL=https://example.org \
    -e DRY_RUN=1 \
    triage-ops
}
```

NOTE:
- You can use `nerdctl` command to run triage-ops with Docker by setting `GITLAB_API_ENDPOINT=http://192.168.5.2:3000/api/v4`.
- The IP 192.168.5.2 comes from [the Lima docs](https://github.com/lima-vm/lima/blob/master/docs/network.md#host-ip-19216852).
- `DRY_RUN=1` allows you to test triage without making API calls to GitLab.
- Remove or set `DRY_RUN=0` if/when you want API calls to be made.
