# frozen_string_literal: true

require_relative '../team_member_helper'
require_relative '../www_gitlab_com'

module Hierarchy
  class PeopleSet
    DEFAULT_ASSIGNEE_FIELDS = %w[
      product_managers
      engineering_managers
      fullstack_managers
      backend_engineering_managers
      frontend_engineering_managers
      software_engineers_in_test
      product_design_managers
    ].freeze
    MENTION_FIELDS = %w[
      backend_engineers
      frontend_engineers
      product_designers
    ].freeze
    PARTICIPANTS_FIELDS = DEFAULT_ASSIGNEE_FIELDS + MENTION_FIELDS

    SLACK_CHANNELS_OVERRIDES = {
      'g_pipeline-authoring' => 'g_pipeline-authoring_alerts',
      'g_pipeline-security' => 'g_pipeline-security_alerts'
    }.freeze

    attr_reader :key, :definition

    # To be overridden
    def self.raw_data
      raise "Not implemented"
    end

    def self.all
      @all ||= raw_data.keys.map { |key| new(key) }
    end

    def self.exist?(key)
      raw_data.key?(key)
    end

    def self.all_labels
      @all_labels ||= all.map(&:label)
    end

    def self.find_by_label(label)
      return if label.nil?

      all.find { |people_set| label.casecmp?(people_set.label) }
    end

    def self.find_by_name(name)
      return if name.nil?

      all.find { |people_set| name.casecmp?(people_set.name) }
    end

    def initialize(key)
      @key = key
      @definition = self.class.raw_data.fetch(key)
    end

    def ==(other)
      key == other&.key
    end

    def slug
      key.gsub(/\W/, '_')
    end

    def name
      definition['name']
    end

    def label
      definition['label']
    end

    def labels
      @labels ||= [label] + extra_labels
    end

    def extra_labels
      definition.fetch('extra_labels', [])
    end

    def slack_channel
      channel = definition['slack_channel']

      SLACK_CHANNELS_OVERRIDES.fetch(channel, channel)
    end

    def assignees(fields = nil)
      TeamMemberHelper.build_unique_mentions(allowed_participants.values_at(*assignee_fields(fields)))
    end

    def mentions(fields = nil)
      return [] if fields

      mentions = allowed_participants.values_at(*MENTION_FIELDS).flatten.compact
      TeamMemberHelper.build_unique_mentions(mentions)
    end

    def to_h(fields = nil)
      {
        assignees: assignees(fields),
        labels: labels,
        mentions: mentions(fields)
      }.compact
    end

    # Can be overridden
    def allowed_participants
      definition.slice(*PARTICIPANTS_FIELDS)
    end

    private

    def assignee_fields(fields)
      if fields
        fields & DEFAULT_ASSIGNEE_FIELDS
      else
        DEFAULT_ASSIGNEE_FIELDS
      end
    end
  end
end
