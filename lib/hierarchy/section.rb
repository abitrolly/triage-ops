# frozen_string_literal: true

require 'yaml'

require_relative './people_set'

module Hierarchy
  class Section < PeopleSet
    def self.raw_data
      # No memoization as it's already cached in MiniCache
      WwwGitLabCom.sections
    end
  end
end
