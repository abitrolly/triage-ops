# frozen_string_literal: true

require_relative '../triage/triage/event'

module CommunityContributionHelper
  CONTRACTOR_GROUP_ID = 62_281_442 # gitlab-org/contractors

  def automation_author?
    ::Triage::Event::AUTOMATION_IDS.include?(resource.dig(:author, :id)) ||
      author.match?(::Triage::Event::GITLAB_SERVICE_ACCOUNT_REGEX)
  end

  def community_or_contractor_contribution_label
    contractor_author? ? 'Contractor Contribution' : 'Community contribution'
  end

  private

  def contractor_author?
    contractor_user_ids.include?(resource.dig(:author, :id))
  end

  def contractor_user_ids
    @contractor_user_ids ||= Triage.fresh_group_members(CONTRACTOR_GROUP_ID).map(&:id)
  end
end
