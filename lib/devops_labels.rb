# frozen_string_literal: true

require_relative 'constants/labels'
require_relative 'hierarchy/group'
require_relative 'hierarchy/stage'
require_relative 'www_gitlab_com'

class DevopsLabels
  CATEGORIES_PER_SPECIAL_GROUPS = {
    'quality' => [
      'failure::broken-test',
      'failure::flaky-test',
      'failure::stale-test',
      'failure::test-environment',
      'master:broken',
      'master:needs-investigation',
      'QA'
    ].freeze,
    'engineering_productivity' => [
      'dev-quality',
      'static analysis'
    ].freeze,
    'infrastructure' => ['infrastructure'].freeze
  }.freeze
  INFRASTRUCTURE_TEAM_LABEL_PREFIX = 'team::'
  INFRASTRUCTURE_TEAM_LABEL_PREFIX_REGEX = /\A#{INFRASTRUCTURE_TEAM_LABEL_PREFIX}/
  CATEGORY_LABEL_PREFIX = 'Category:'
  CATEGORY_LABEL_PREFIX_REGEX = /\A#{CATEGORY_LABEL_PREFIX}/
  WORKFLOW_LABEL_PREFIX = 'workflow::'
  WORKFLOW_LABEL_PREFIX_REGEX = /\A#{WORKFLOW_LABEL_PREFIX}/
  DISABLED_SINGLE_CATEGORY_TO_GROUP_INFERENCE = [
    'Category:Activation',
    'Category:Service Ping',
    'Category:GitLab Advisory Database' # The Vulnerability research group works on a lot of categories that are under of other groups
  ].freeze
  TRIAGE_MISSING_CATEGORIES_PROJECTS = [
    278964, # gitlab-org/gitlab
    250833, # gitlab-org/gitlab-runner
    1441932 # gitlab-org/gitlab-qa
  ].freeze
  MISSING_PRODUCT_GROUP_LABEL = 'missing product_group_label'
  MISSING_FEATURE_CATEGORY_LABEL = 'missing feature_category label'

  def self.category_labels_per_group
    @category_labels_per_group ||=
      WwwGitLabCom.categories.each_with_object(Hash.new { |h, k| h[k] = [] }) do |(_, attrs), memo|
        group_key = attrs['group']
        memo[group_key] << attrs['label']
        memo[group_key].concat(attrs.fetch('feature_labels', []))
        memo[group_key].uniq!
        memo[group_key].flatten!
      end
  end

  def self.group_category_labels_per_category(category_key)
    category_data = WwwGitLabCom.categories[category_key]

    return unless category_data&.fetch('group')

    group = Hierarchy::Group.new(category_data.fetch('group'))

    {
      'category_label' => category_data['label'],
      'group_label' => group.label
    }
  end

  def self.category_labels_per_group_or_department
    @category_labels_per_group_or_department ||= category_labels_per_group.merge(CATEGORIES_PER_SPECIAL_GROUPS)
  end

  def self.category_labels
    @category_labels ||= (category_labels_per_group.values + CATEGORIES_PER_SPECIAL_GROUPS.values).flatten
  end

  def self.group_for_user(username)
    return if username.nil? || username.empty?

    groups = groups_per_user(username)
    return unless groups.one?

    groups.first
  end

  def self.groups_per_user(username)
    gitlab_team_member = WwwGitLabCom.team_from_www[username]
    return [] if gitlab_team_member.nil?

    departments = gitlab_team_member['departments']
    return [] if departments.nil?

    teams = departments.select { |dept| dept.end_with?('Team') }
    sanitize_team_names(teams)
  end
  private_class_method :groups_per_user

  def self.sanitize_team_names(teams)
    teams.flat_map do |team|
      team_name = team.delete_suffix(' Team').downcase
      team_name = drop_role(team_name)
      team_name = drop_stage(team_name)

      matching_group = Hierarchy::Group.find_by_name(team_name)
      next unless matching_group

      matching_group
    end.uniq
  end
  private_class_method :sanitize_team_names

  def self.drop_role(team_name)
    %w[ux be fe pm].each do |role|
      return team_name.delete_suffix(" #{role}") if team_name.end_with?(role)
    end

    team_name
  end
  private_class_method :drop_role

  def self.drop_stage(team_name)
    Hierarchy::Stage.all.each do |stage|
      downcase_name = stage.name.downcase
      return team_name.delete_prefix("#{downcase_name}:") if team_name.start_with?(downcase_name)
    end

    team_name
  end
  private_class_method :drop_stage

  module Context
    extend self

    LabelsInferenceResult = Class.new(Struct.new(:new_labels)) do
      def any?
        new_labels.any?
      end

      def new_labels
        Array(self[:new_labels])
          .flatten
          .compact
          .reject(&:blank?)
          .uniq
          .sort
      end

      def comment
        return unless any?

        quick_actions
      end

      def quick_actions
        "/label #{new_labels_markdown}"
      end

      def merge(other_inference_result)
        self.class.merge(self, other_inference_result)
      end

      def self.merge(*label_inference_results)
        new_labels = label_inference_results.map(&:new_labels)

        new(new_labels)
      end

      def self.empty
        new
      end

      private

      def new_labels_markdown
        new_labels.map { |label| Context.markdown_label(label) }.join(' ')
      end
    end

    MatchingGroup = Struct.new(:key, :matching_labels)

    def label_names
      @label_names ||= labels.map(&:name)
    end

    def find_scoped_label(labels_array, prefix)
      labels_array.grep(prefix).max_by(&:size)
    end

    def detect_hierarchy_label(klass, labels)
      (Hierarchy.const_get(klass).all_labels & labels).first
    end

    def stage_name(labels)
      Hierarchy::Stage.find_by_label(detect_hierarchy_label('Stage', labels))&.key
    end

    def group_name(labels)
      Hierarchy::Group.find_by_label(detect_hierarchy_label('Group', labels))&.key
    end

    def current_section_label
      @current_section_label ||= detect_hierarchy_label('Section', label_names)
    end

    def current_stage_label
      @current_stage_label ||= detect_hierarchy_label('Stage', label_names)
    end

    def current_group_label
      @current_group_label ||= detect_hierarchy_label('Group', label_names)
    end

    def current_infrastructure_team_label
      find_scoped_label(label_names, INFRASTRUCTURE_TEAM_LABEL_PREFIX_REGEX)
    end

    def current_workflow_label
      find_scoped_label(label_names, WORKFLOW_LABEL_PREFIX_REGEX)
    end

    def current_category_labels
      DevopsLabels.category_labels & label_names
    end

    def current_stage
      Hierarchy::Stage.find_by_label(current_stage_label)
    end

    def current_group
      Hierarchy::Group.find_by_label(current_group_label)
    end

    def current_stage_key
      current_stage&.key
    end

    def current_group_key
      current_group&.key
    end

    def all_category_labels_for_current_stage
      return [] unless current_stage

      current_stage.groups.flat_map do |group|
        all_category_labels_for_group(group)
      end
    end

    def all_category_labels_for_group(group)
      return [] unless group

      DevopsLabels.category_labels_per_group.fetch(group.key, [])
    end

    def section_for_stage(stage_key)
      return unless Hierarchy::Stage.exist?(stage_key)

      Hierarchy::Stage.new(stage_key).section
    end

    def section_for_group(group_key)
      return unless Hierarchy::Group.exist?(group_key)

      Hierarchy::Group.new(group_key).section
    end

    def has_section_label?
      !current_section_label.nil?
    end

    def has_stage_label?
      !current_stage_label.nil?
    end

    def has_group_label?
      !current_group_label.nil?
    end

    def has_specialization_label?
      ([Labels::FRONTEND_LABEL, Labels::BACKEND_LABEL] & label_names).any?
    end

    def has_category_label?
      current_category_labels.any?
    end

    def has_category_label_for_current_stage?
      return false unless has_stage_label?

      !(all_category_labels_for_current_stage & label_names).empty?
    end

    def has_category_label_for_current_group?
      return false unless has_group_label?

      !(all_category_labels_for_group(current_group) & label_names).empty?
    end

    def workflow_label_is?(labels)
      Array(labels).include?(current_workflow_label)
    end

    def can_infer_labels?
      return false if has_section_label? &&
        has_stage_label? &&
        has_group_label? &&
        has_category_label_for_current_group?

      has_stage_label? ||
        has_group_label? ||
        has_category_label?
    end

    def can_add_default_group?
      !has_group_label? &&
        current_infrastructure_team_label.nil?
    end

    def can_add_default_group_and_stage?
      can_add_default_group? && !has_stage_label?
    end

    def can_infer_group_from_author?
      !!DevopsLabels.group_for_user(author)
    end

    def current_stage_has_a_single_group?
      return false unless current_stage

      current_stage.groups.one?
    end

    def group_part_of_stage?
      return false unless current_stage
      return false unless current_group

      current_stage.groups.include?(current_group)
    end

    def first_group_for_current_stage
      return unless current_stage

      current_stage.groups.first
    end

    def stage_for_group(group_key)
      return unless Hierarchy::Group.exist?(group_key)

      Hierarchy::Group.new(group_key).stage
    end

    def comment_for_intelligent_stage_and_group_labels_inference
      new_stage_and_group_labels = infer_new_labels

      inferred_labels = infer_section_label(new_stage_and_group_labels)

      inferred_labels.comment
    end

    def comment_for_mr_author_group_label
      group = DevopsLabels.group_for_user(author)
      return if group.nil?

      LabelsInferenceResult.new(group.labels).comment
    end

    def markdown_label(label)
      %(~"#{label}")
    end

    private

    def infer_new_labels
      return LabelsInferenceResult.empty if has_stage_label? && has_group_label? && has_category_label_for_current_group?

      if has_stage_label?
        if has_group_label?
          infer_category_from_group
        else
          infer_group_from_category
        end
      elsif has_group_label?
        infer_stage_and_category_from_group
      else
        infer_stage_and_group_from_category
      end
    end

    def infer_group_from_stage
      return LabelsInferenceResult.empty unless current_stage_has_a_single_group?

      LabelsInferenceResult.new(first_group_for_current_stage.labels)
    end

    def infer_group_from_category
      matching_group_key = best_matching_group&.key
      return infer_group_from_stage unless Hierarchy::Group.exist?(matching_group_key)

      LabelsInferenceResult.new(Hierarchy::Group.new(matching_group_key).labels)
    end

    def infer_stage_and_group_from_category
      matching_group_key = best_matching_group&.key
      return LabelsInferenceResult.empty unless Hierarchy::Group.exist?(matching_group_key)

      stage_labels = stage_for_group(matching_group_key).labels
      group_labels = Hierarchy::Group.new(matching_group_key).labels

      LabelsInferenceResult.new(stage_labels + group_labels)
    end

    def infer_category_from_group
      category_labels = all_category_labels_for_group(current_group).filter do |category|
        category.match?(DevopsLabels::CATEGORY_LABEL_PREFIX_REGEX)
      end

      if category_labels.one? && !current_category_labels.include?(category_labels.first) && !DISABLED_SINGLE_CATEGORY_TO_GROUP_INFERENCE.include?(category_labels.first)
        LabelsInferenceResult.new(category_labels.first)
      else
        LabelsInferenceResult.empty
      end
    end

    def infer_stage_from_group
      LabelsInferenceResult.new(stage_for_group(current_group_key).labels)
    end

    def infer_stage_and_category_from_group
      inferred_stage = infer_stage_from_group
      inferred_category = infer_category_from_group

      LabelsInferenceResult.merge(inferred_stage, inferred_category)
    end

    def infer_section_label(label_inference_result)
      # Already having a section label
      return label_inference_result if has_section_label?

      # Prefer the new labels over the current labels because new scoped
      # labels will replace the current scoped label
      eventual_labels = label_inference_result.new_labels | label_names

      section_from_group = section_for_group(group_name(eventual_labels))
      section_from_stage = section_for_stage(stage_name(eventual_labels))

      # Nothing inferred
      return label_inference_result if section_from_group.nil? && section_from_stage.nil?

      # Ambiguous, different sections inferred from group and stage! Ignore
      # It may be a group under a different section works on a stage under
      # another section. See:
      # https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#naming-and-color-convention-1
      return label_inference_result if
        section_from_group && section_from_stage &&
          section_from_group != section_from_stage

      label_inference_result.merge(
        LabelsInferenceResult.new(section_from_group&.label || section_from_stage&.label)
      )
    end

    def best_matching_group
      @best_matching_group ||= begin
        matching_labels_count = category_matches_for_current_stage.flat_map(&:matching_labels).size.to_f

        category_matches_for_current_stage.find do |matching_group|
          (matching_group.matching_labels.size / matching_labels_count) > 0.5
        end
      end
    end

    def category_matches_for_current_stage
      @category_matches_for_current_stage ||=
        DevopsLabels.category_labels_per_group_or_department.each_with_object([]) do |(group, group_category_labels), matches|
          next if has_stage_label? && current_stage != stage_for_group(group)

          matching_category_labels = group_category_labels & current_category_labels
          next if matching_category_labels.empty?

          matches << MatchingGroup.new(group, matching_category_labels)
        end
    end
  end
end
