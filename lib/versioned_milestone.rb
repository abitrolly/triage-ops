# frozen_string_literal: true
require 'gitlab/triage/engine'

VersionedMilestone = Struct.new(:context) do
  def current
    all_non_expired.first
  end

  def next
    all_non_expired[1]
  end

  def find_milestone_for_date(date)
    all_active_with_start_date.find do |milestone|
      milestone.due_date && milestone.in_progress?(date)
    end
  end

  def all_active_with_start_date
    @all_active_with_start_date ||=
      root_milestone.__send__(:all_active_with_start_date).select do |m|
        m.title.match?(/\A\d+\.\d+\z/) # So we don't pick 2019
      end
  end

  # We want to look into non-expired, including which aren't started yet
  def all_non_expired
    @all_non_expired ||=
      all_active_with_start_date.reject(&:expired?)
  end

  def current?(other)
    other&.title == current.title
  end

  private

  def root_milestone
    @root_milestone ||= Gitlab::Triage::Resource::Milestone.new(
      { group_id: context.root_id },
      parent: context,
      redact_confidentials: false
    )
  end
end
