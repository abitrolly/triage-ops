# frozen_string_literal: true

require 'date'

module MilestoneHelper
  def set_milestone_for_merge_request(merge_request)
    milestone = milestone_for_merge_request(merge_request)
    return unless milestone

    %(/milestone %"#{milestone.title}")
  end

  def next_milestone
    VersionedMilestone.new(self).next
  end

  private

  def milestone_for_merge_request(merge_request)
    return unless merge_request[:merged_at]

    VersionedMilestone.new(self).find_milestone_for_date(Date.parse(merge_request[:merged_at]))
  end
end
