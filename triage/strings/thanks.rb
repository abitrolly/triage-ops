# frozen_string_literal: true

require_relative '../../lib/constants/labels'

module Triage
  module Strings
    module Thanks
      READY_FOR_REVIEW = "* When you're ready for a review, comment on this merge request with @gitlab-bot ready."
      REQUEST_HELP = '* If you need help, comment @gitlab-bot help or come say hi on [Discord](https://discord.gg/gitlab).'
      AI_CHECKBOX = '* We welcome AI-generated contributions! Read more/check the box at the top of the merge request description.'
      GROUP_LABEL = '* To [add labels to your merge request](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations/#reactive-label-and-unlabel-commands), comment @gitlab-bot label ~"label1" ~"label2".'

      RUNNER_BODY = <<~MARKDOWN.chomp
        Some contributions require several iterations of review and we try to mentor contributors
        during this process. However, we understand that some reviews can be very time consuming.
        If you would prefer for us to continue the work you've submitted now or at any point in the
        future please let us know.

        If you're okay with being part of our review process (and we hope you are!), there are
        several initial checks we ask you to make:

        * The merge request description clearly explains:
          * The problem being solved.
          * The best way a reviewer can test your changes (is it possible to provide an example?).
        * If the pipeline failed, do you need help identifying what failed?
        * Check that Go code follows our [Go guidelines](https://docs.gitlab.com/ee/development/go_guide/index.html#code-review).
        * Read our [contributing to GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/CONTRIBUTING.md#contribute-to-gitlab-runner)
        document.
      MARKDOWN

      GITLAB_FOSS_BODY = <<~MARKDOWN.chomp
        Hey @%<author_username>s! :wave:

        Thank you for your contribution. GitLab has moved to a single codebase for GitLab CE and GitLab EE.

        Please do not create merge requests here. Instead, create them at https://gitlab.com/gitlab-org/gitlab/-/merge_requests.

        /close
      MARKDOWN

      WWW_GITLAB_COM_BODY = <<~MARKDOWN.chomp
        I'll notify the website team about your merge request and they will get back to you as soon
        as they can.
        If you don't hear from someone in a reasonable amount of time, please ping us again in a
        comment and mention @gitlab-com-community.
      MARKDOWN

      INTRO_THANKS_FOR_COMMUNITY_FORK = <<~MARKDOWN.chomp
        Thanks for your contribution to GitLab @%<author_username>s! :tada:
      MARKDOWN

      SIGNOFF_THANKS = <<~MARKDOWN.chomp
        /label ~"#{Labels::COMMUNITY_CONTRIBUTION_LABEL}" ~"#{Labels::WORKFLOW_IN_DEV_LABEL}"
        /assign @%<author_username>s
      MARKDOWN

      COMMUNITY_FORKS = <<~MARKDOWN.chomp
        Did you know about our [community forks](https://gitlab.com/gitlab-community/meta)?
        Working from there will make your contribution process easier. Please check it out!
      MARKDOWN

      INTRO_THANKS_DEFAULT = <<~MARKDOWN.chomp
        #{INTRO_THANKS_FOR_COMMUNITY_FORK}

        #{COMMUNITY_FORKS}
      MARKDOWN

      FIRST_SIGNOFF_THANKS = <<~MARKDOWN.chomp
        #{SIGNOFF_THANKS}
        /label ~"#{Labels::FIRST_CONTRIBUTION_LABEL}"
      MARKDOWN
    end
  end
end
