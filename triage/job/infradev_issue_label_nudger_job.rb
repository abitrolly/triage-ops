# frozen_string_literal: true

require_relative '../triage/job'
require_relative '../../triage/triage/infradev_issue_label_validator'

module Triage
  class InfradevIssueLabelNudgerJob < Job
    include Reaction

    private

    def execute(event)
      prepare_executing_with(event)
      return unless InfradevIssueLabelValidator.new(event).applicable_issue?

      add_comment(comment_body, append_source_link: true)
    end

    def comment_body
      comment = <<~MARKDOWN.chomp
        :wave: @#{event.resource_author.username} - This ~"infradev" issue is missing a group label and is at risk of missing its SLO due date.

        Please apply a group label now following the [Triage Process](https://about.gitlab.com/handbook/engineering/workflow/#triage-process) handbook page section.

        You can ping one of the [Engineering Directors](https://about.gitlab.com/handbook/engineering/development/#managers-and-directors) if you require assistance.
      MARKDOWN

      unique_comment.wrap(comment).strip
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new(InfradevIssueLabelValidator::INFRADEV_LABEL_NUDGER_CLASS, event)
    end
  end
end
