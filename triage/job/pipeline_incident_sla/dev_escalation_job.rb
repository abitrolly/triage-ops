# frozen_string_literal: true

require_relative './base_job'
require_relative '../../triage/reaction'

module Triage
  module PipelineIncidentSla
    class DevEscalationJob < BaseJob
      include Reaction

      DELAY = 2 * 60 * 60 # 2 hours
      PAGERSLACK_MEMBER_ID = 'U014GHS7QV9'

      private

      def execute(event)
        super

        add_comment('/label ~"escalated"', append_source_link: false) if applicable?
      end

      def threshold_in_seconds
        DELAY
      end

      def slack_channel
        PAGERSLACK_MEMBER_ID
      end

      def slack_message
        "devoncall #{event.url}"
      end
    end
  end
end
