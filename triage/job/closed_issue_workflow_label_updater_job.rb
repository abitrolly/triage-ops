# frozen_string_literal: true

require_relative '../triage/job'

module Triage
  class ClosedIssueWorkflowLabelUpdaterJob < Job
    include Reaction

    private

    def execute(event)
      prepare_executing_with(event)
      post_comment_and_update_workflow_label if applicable?
    end

    def applicable?
      issue.state == 'closed' && issue.labels.include?(Labels::WORKFLOW_VERIFICATION)
    end

    def issue
      @issue ||= Triage.api_client.issue(event.project_id, event.iid)
    end

    def post_comment_and_update_workflow_label
      comment = <<~MARKDOWN.chomp
        The workflow label was automatically updated to ~"#{Labels::WORKFLOW_COMPLETE}" because you closed the issue while in ~"#{Labels::WORKFLOW_VERIFICATION}".

        If this is not the correct label, please update.

        To avoid this message, update the workflow label as you close the issue.
        /label ~"#{Labels::WORKFLOW_COMPLETE}"
      MARKDOWN

      add_comment(comment, append_source_link: true)
    end
  end
end
