# frozen_string_literal: true

require_relative '../../lib/constants/labels'
require_relative '../job/trigger_pipeline_on_approval_job'
require_relative '../triage/event'
require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require_relative '../triage/need_mr_approved_label'
require_relative 'label_jihu_contribution'

module Triage
  class NewPipelineOnApproval < Processor
    FIVE_SECONDS = 5
    SUPPORTED_PROJECT_IDS = [
      Triage::Event::GITLAB_PROJECT_ID,
      Triage::Event::TF_PROVIDER_GITLAB_PROJECT_ID
    ].freeze
    NewPipelineMessage = Struct.new(:event, :trigger_pipeline_automatically?) do
      def to_s
        <<~MARKDOWN.strip
          :wave: `@#{event.event_actor_username}`, thanks for approving this merge request.

          This is the first time the merge request has been approved.
          #{call_to_action}

          Please wait for the pipeline to start before resolving this discussion and set auto-merge for the new pipeline.
          See [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request) for more details.

          /label ~"#{Labels::MR_APPROVED_LABEL}"
        MARKDOWN
      end

      def call_to_action
        sentence_start = "To ensure we don't only run [predictive pipelines](https://docs.gitlab.com/ee/development/pipelines/index.html#predictive-test-jobs-before-a-merge-request-is-approved), and we don't break `master`"

        if trigger_pipeline_automatically?
          "#{sentence_start}, a new pipeline will be started shortly."
        else
          "#{sentence_start}, please start a new pipeline before merging."
        end
      end
    end

    react_to_approvals

    def applicable?
      SUPPORTED_PROJECT_IDS.include?(event.project_id) &&
        need_mr_approved_label? &&
        unique_comment.no_previous_comment?
    end

    def process
      trigger_pipeline_automatically = event.team_member_author? || event.automation_author?

      message = unique_comment.wrap(
        NewPipelineMessage.new(event, trigger_pipeline_automatically))

      trigger_merge_request_pipeline if trigger_pipeline_automatically

      add_discussion(message, append_source_link: false)
    end

    def documentation
      <<~TEXT
        This processor triggers a pipeline run on a newly approved merge request.
      TEXT
    end

    private

    def trigger_merge_request_pipeline
      TriggerPipelineOnApprovalJob.perform_in(FIVE_SECONDS, event.noteable_path)
    end

    def need_mr_approved_label?
      NeedMrApprovedLabel.new(event).need_mr_approved_label?
    end
  end
end
