# frozen_string_literal: true

require_relative '../triage/processor'

require 'digest'
require 'slack-messenger'

module Triage
  class UxPaperCutsMrs < Processor
    UX_PAPER_CUTS_LABEL = 'UX Paper Cuts'
    SLACK_ICON = ':robot_face:'
    SLACK_CHANNEL = '#ux_paper_cuts_mrs'
    SLACK_MESSAGE_TEMPLATE = <<~MESSAGE
      An MR from the UX Paper Cuts team was just merged 🎉 (%<mr_title>s) - %<mr_url>s.
    MESSAGE

    react_to 'merge_request.merge'

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def applicable?
      event.from_gitlab_org? && ux_paper_cuts_label_added?
    end

    def process
      post_merged_mr_to_slack
    end

    def slack_options
      {
        channel: SLACK_CHANNEL,
        username: GITLAB_BOT,
        icon_emoji: SLACK_ICON
      }
    end

    private

    attr_reader :messenger

    def ux_paper_cuts_label_added?
      event.label_names.include?(UX_PAPER_CUTS_LABEL)
    end

    def post_merged_mr_to_slack
      slack_message = format(SLACK_MESSAGE_TEMPLATE, mr_url: event.url, mr_title: event.title)
      messenger.ping(slack_message)

      true
    end

    def slack_messenger
      Slack::Messenger.new(ENV.fetch('SLACK_WEBHOOK_URL', nil), slack_options)
    end
  end
end
