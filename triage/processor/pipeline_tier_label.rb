# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/need_mr_approved_label'

module Triage
  class PipelineTierLabel < Processor
    react_to 'merge_request.open',
      'merge_request.approval',
      'merge_request.approved',
      'merge_request.unapproval',
      'merge_request.unapproved',
      'merge_request.update' # We don't send unapproval/unapproved webhook events when resetting MR approvals from the product.

    def applicable?
      applicable_project? &&
        !event.gitlab_bot_event_actor? &&
        !pipeline_expedited? &&
        need_pipeline_tier_label?
    end

    def process
      label_to_apply =
        if no_approvals?
          Labels::PIPELINE_TIER_1_LABEL
        elsif partially_approved?
          Labels::PIPELINE_TIER_2_LABEL
        elsif fully_approved?
          Labels::PIPELINE_TIER_3_LABEL
        end

      return if label_to_apply.nil? || label_already_present?(label_to_apply)

      comment = "/label ~\"#{label_to_apply}\""
      add_comment(comment, append_source_link: false)
    end

    def documentation
      <<~TEXT
        Ensures that a ~"pipeline::tier-X" label is set (X is a number between 1 and 3) with the following rules:
         - if the MR has no approvals => ~"pipeline::tier-1"
         - if the MR has at at least one approval, but still requires more approvals => ~"pipeline::tier-2"
         - if the MR has all the approvals it needs => ~"pipeline::tier-3"
      TEXT
    end

    private

    def applicable_project?
      event.from_gitlab_org_gitlab? || event.from_gitlab_org_security_gitlab?
    end

    def no_approvals?
      event.approvers.count.zero?
    end

    def partially_approved?
      !no_approvals? && event.approvals.approvals_left.positive?
    end

    def fully_approved?
      event.approvals.approved
    end

    def label_already_present?(label)
      event.label_names.include?(label)
    end

    def pipeline_expedited?
      event.label_names.include?(Labels::PIPELINE_EXPEDITED_LABEL) ||
        # TODO: Remove once the label is renamed to be scoped
        event.label_names.include?(Labels::PIPELINE_LEGACY_EXPEDITE_LABEL)
    end

    def need_pipeline_tier_label?
      # TODO: When retiring the mr-approved label, rename this class and its methods to NeedPipelineTierLabel
      NeedMrApprovedLabel.new(event).need_mr_approved_label?
    end
  end
end
