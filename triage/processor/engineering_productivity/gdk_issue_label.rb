# frozen_string_literal: true

require_relative '../../triage/event'
require_relative '../../triage/processor'

module Triage
  class GdkIssueLabel < Processor
    react_to 'issue.open', 'issue.update'

    CHECKBOX_SELECTION_REGEX = /\[x\] ~"(gdk-reliability|gdk-usability|gdk-performance)/
    GDK_CATEGORY_LABELS = %w[gdk-reliability gdk-usability gdk-performance].freeze

    def applicable?
      event.with_project_id?(Triage::Event::GDK_PROJECT_ID) && !category_labels_match?
    end

    def documentation
      'Categorizes GDK issue by applying label based on checkbox selection in issue description'
    end

    def process
      update_labels
    end

    private

    def selected_categories
      event.description.scan(CHECKBOX_SELECTION_REGEX).flatten
    end

    def category_labels_match?
      labels_to_remove.empty? && labels_to_add.empty?
    end

    def current_gdk_labels
      event.label_names.select { |label| GDK_CATEGORY_LABELS.include?(label) }
    end

    def update_labels
      comments = []
      comments.push("/label #{labels_to_markdown(labels_to_add)}") if labels_to_add.any?
      comments.push("/unlabel #{labels_to_markdown(labels_to_remove)}") if labels_to_remove.any?

      add_comment(comments.join("\n"), append_source_link: false) unless comments.empty?
    end

    def labels_to_markdown(label_names)
      label_names.map { |label_name| %(~"#{label_name}") }.join(' ')
    end

    def labels_to_add
      selected_categories - current_gdk_labels
    end

    def labels_to_remove
      current_gdk_labels - selected_categories
    end
  end
end
