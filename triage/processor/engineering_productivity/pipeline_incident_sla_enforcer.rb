# frozen_string_literal: true

require_relative '../../../lib/constants/labels'
require_relative '../../triage/event'
require_relative '../../triage/processor'
require_relative '../../job/pipeline_incident_sla/group_reminder_job'
require_relative '../../job/pipeline_incident_sla/group_warning_job'
require_relative '../../job/pipeline_incident_sla/stage_warning_job'
require_relative '../../job/pipeline_incident_sla/dev_escalation_job'

module Triage
  class PipelineIncidentSlaEnforcer < Processor
    react_to 'incident.open'

    AUTO_TRIAGE_LATENCY_SECONDS = 30 # incidents can have a slightly later updated_at than created_at due to auto triage delay

    def applicable?
      event.from_master_broken_incidents_project? &&
        event.gitlab_bot_event_actor? &&
        event.label_names.include?(Labels::MASTER_BROKEN_LABEL)
    end

    def process
      Triage::PipelineIncidentSla::GroupReminderJob.perform_in(Triage::PipelineIncidentSla::GroupReminderJob::DELAY + AUTO_TRIAGE_LATENCY_SECONDS, event)
      Triage::PipelineIncidentSla::GroupWarningJob.perform_in(Triage::PipelineIncidentSla::GroupWarningJob::DELAY + AUTO_TRIAGE_LATENCY_SECONDS, event)
      Triage::PipelineIncidentSla::StageWarningJob.perform_in(Triage::PipelineIncidentSla::StageWarningJob::DELAY + AUTO_TRIAGE_LATENCY_SECONDS, event)
      Triage::PipelineIncidentSla::DevEscalationJob.perform_in(Triage::PipelineIncidentSla::DevEscalationJob::DELAY + AUTO_TRIAGE_LATENCY_SECONDS, event)
    end

    def documentation
      <<~TEXT
        This processor reminds group member to triage pipeline incidents within SLA and escalate in 3 hours of inactivity.
      TEXT
    end
  end
end
