# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require_relative 'label_jihu_contribution'

module Triage
  class AppSecProcessor < Processor
    APPSEC_APPROVAL_LABEL = 'sec-planning::complete'
    APPSEC_PENDING_APPROVAL_LABEL = 'sec-planning::pending-followup'

    def applicable?
      event.from_gitlab_org? &&
        !event.gitlab_bot_event_actor? &&
        (has_jihu_contribution_label? || event.jihu_contributor?)
    end

    private

    def approved_by_appsec?(user_id = event.event_actor_id)
      Triage.gitlab_com_appsec_member_ids.include?(user_id)
    end

    def has_appsec_approval_label?
      event.label_names.include?(APPSEC_APPROVAL_LABEL)
    end

    def has_jihu_contribution_label?
      event.label_names.include?(LabelJiHuContribution::LABEL_NAME)
    end

    def add_to_appsec_thread(message)
      if appsec_discussion_thread
        discussion_id = appsec_discussion_thread['id']

        append_discussion(message, discussion_id, append_source_link: false)
      else
        add_discussion(message, append_source_link: false)

        discussion_id = appsec_discussion_thread['id']
      end

      discussion_id
    end

    def unique_thread
      @unique_thread ||= UniqueComment.new(unique_thread_key, event)
    end

    def appsec_discussion_thread
      @appsec_discussion_thread ||= unique_thread.previous_discussion
    end

    def unique_thread_key
      # To make this backward compatible, we use this name.
      # At some point if we're ok to break compatibility, we can use
      # 'Triage::AppSecProcessor'
      'Triage::PingAppSecOnApproval'
      # Note that this should not be `self.class.name` because it'll be
      # dynamic for the subclasses.
    end
  end
end
