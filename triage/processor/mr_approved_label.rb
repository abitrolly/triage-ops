# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/need_mr_approved_label'

module Triage
  class MrApprovedLabel < Processor
    react_to 'merge_request.approval',
      'merge_request.approved',
      'merge_request.unapproval',
      'merge_request.unapproved',
      'merge_request.update' # We don't send unapproval/unapproved webhook events when resetting MR approvals from the product.

    def applicable?
      applicable_project? && approved_at_least_once? && need_mr_approved_label?
    end

    def process
      comment = "/label ~\"#{Labels::MR_APPROVED_LABEL}\""

      add_comment(comment, append_source_link: false)
    end

    def documentation
      <<~TEXT
        Ensures that the ~"pipeline:mr-approved" label is set if the MR has at least one approval.
      TEXT
    end

    private

    def applicable_project?
      event.from_gitlab_org_gitlab? || event.from_gitlab_org_security_gitlab?
    end

    def approved_at_least_once?
      event.approvers.count.positive?
    end

    def need_mr_approved_label?
      NeedMrApprovedLabel.new(event).need_mr_approved_label?
    end
  end
end
