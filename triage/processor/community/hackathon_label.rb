# frozen_string_literal: true

require 'time'

require_relative 'community_processor'

module Triage
  class HackathonLabel < CommunityProcessor
    HACKATHON_START_DATE = Time.parse('2024-08-26T00:00:00Z')
    HACKATHON_END_DATE = Time.parse('2024-09-02T00:00:00Z')
    HACKATHON_TRACKING_ISSUE = 'https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/514'
    HACKATHON_WEBPAGE = 'https://about.gitlab.com/community/hackathon/'

    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      wider_community_contribution_open_resource? &&
        created_during_hackathon? &&
        # Ensure we don't post the note twice since we only introduced `unique_comment` recently.
        # This could be removed after some time.
        no_hackathon_label? &&
        unique_comment.no_previous_comment?
    end

    def process
      label_hackathon
    end

    def documentation
      <<~TEXT
        This procesor applies the ~"Hackathon" label to open source merge requests created during a hackathon.
      TEXT
    end

    private

    def label_hackathon
      comment = <<~MARKDOWN.chomp
        This merge request will be counted as part of the [running Hackathon](#{HACKATHON_TRACKING_ISSUE})!

        Find yourself in the [Hackathon Leaderboard](https://gitlab-community.gitlab.io/community-projects/merge-request-leaderboard)
        or check out the [Hackathon page](#{HACKATHON_WEBPAGE}) for more information! :tada:

        /label ~"#{Labels::HACKATHON_LABEL}"
      MARKDOWN
      add_comment(unique_comment.wrap(comment), append_source_link: true)
    end

    def no_hackathon_label?
      !event.label_names.include?(Labels::HACKATHON_LABEL)
    end

    def created_during_hackathon?
      event.created_at >= HACKATHON_START_DATE &&
        event.created_at < HACKATHON_END_DATE
    end
  end
end
