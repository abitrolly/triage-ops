# frozen_string_literal: true

require_relative 'community_processor'

module Triage
  class FirstContributionMergedMessage < CommunityProcessor
    react_to 'merge_request.merge'

    def applicable?
      wider_community_contribution? &&
        first_contribution? &&
        unique_comment.no_previous_comment? &&
        event.not_spam?
    end

    def process
      post_first_contribution_comment
    end

    def documentation
      <<~TEXT
        This processor sends a message to contributors after merging their first contribution.
      TEXT
    end

    private

    def post_first_contribution_comment
      add_comment(message.strip, append_source_link: true)
    end

    def message
      comment = <<~MARKDOWN.chomp
        @#{event.resource_author.username}, congratulations for getting your first MR merged :tada:

        If this is your first MR against a GitLab project, we'd like to invite and encourage you to self-nominate yourself for `First MR Merged` swag prize [here](https://docs.google.com/forms/d/e/1FAIpQLSfGo-3kEimVPpC5zKKxXHkFjgYx8-vQAanzAX2LxGgXQqXikQ/viewform).

        Thank you again for contributing, what's your next contribution going to be? :thinking:
      MARKDOWN

      unique_comment.wrap(comment)
    end

    def first_contribution?
      event.first_contribution?
    end

    def not_spam?
      !event.label_names.include?(Labels::SPAM_LABEL)
    end
  end
end
