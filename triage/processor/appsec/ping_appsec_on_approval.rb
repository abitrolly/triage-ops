# frozen_string_literal: true

require_relative '../appsec_processor'
require_relative '../label_jihu_contribution'

module Triage
  class PingAppSecOnApproval < AppSecProcessor
    react_to_approvals

    def applicable?
      super &&
        !has_appsec_approval_label? &&
        unique_comment.no_previous_comment?
    end

    def process
      add_to_appsec_thread(message_to_post)
    end

    def documentation
      <<~TEXT
        This processor pings the AppSec team when a jihu merge request has been approved without the ~"sec-planning::complete" label.
      TEXT
    end

    private

    def message_to_post
      @message_to_post ||=
        unique_thread.wrap(
          unique_comment.wrap(PingAppSecMessage.new(event)))
    end

    def unique_comment
      @unique_comment ||=
        UniqueComment.new(
          'Triage::PingAppSecOnApprovalMarker',
          event,
          from: Event::GITLAB_BOT_USERNAME)
    end

    PingAppSecMessage = Struct.new(:event) do
      def to_s
        <<~MARKDOWN.strip
          #{header}

          #{body}

          #{request_appsec_review}
        MARKDOWN
      end

      def header
        <<~HEADER.chomp
          :wave: `@#{event.event_actor_username}`, thanks for approving this merge request.
        HEADER
      end

      def body
        <<~BODY.chomp
          This is the first time the merge request has been approved. Please wait for AppSec approval before merging.
        BODY
      end

      def request_appsec_review
        <<~MARKDOWN.chomp
          cc @#{Triage::GITLAB_COM_APPSEC_GROUP} this is a ~"#{Triage::LabelJiHuContribution::LABEL_NAME}", please follow the [JiHu contribution review process](https://handbook.gitlab.com/handbook/ceo/office-of-the-ceo/jihu-support/jihu-security-review-process/#security-review-workflow-for-jihu-contributions).
        MARKDOWN
      end
    end
  end
end
