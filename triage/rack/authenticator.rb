# frozen_string_literal: true

require 'rack'
require 'digest/sha2'

module Triage
  module Rack
    Authenticator = Struct.new(:app) do
      def self.secure_compare(a, b)
        ::Rack::Utils.secure_compare(
          ::OpenSSL::Digest::SHA256.hexdigest(a),
          ::OpenSSL::Digest::SHA256.hexdigest(b)) &&
          a == b
      end

      def call(env)
        return ::Rack::Response.new([JSON.dump({ status: :unauthenticated, message: "Token wasn't provided" })], 400).finish if webhook_token_empty?

        if authenticated?(env)
          app.call(env)
        else
          ::Rack::Response.new([JSON.dump({ status: :unauthenticated })], 401).finish
        end
      end

      private

      def authenticated?(env)
        self.class.secure_compare(http_gitlab_token(env), webhook_token)
      end

      def http_gitlab_token(env)
        env['HTTP_X_GITLAB_TOKEN'].to_s
      end

      def webhook_token
        ENV['GITLAB_WEBHOOK_TOKEN'].to_s
      end

      def webhook_token_empty?
        webhook_token.match?(/\A\s*\z/)
      end
    end
  end
end
