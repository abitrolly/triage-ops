# frozen_string_literal: true

require_relative '../triage'
require_relative 'changed_file_list'
require_relative '../../lib/constants/labels'

module Triage
  class NeedMrApprovedLabel
    SKIP_WHEN_CHANGES_ONLY_REGEX = %r{\A(?:docs?|qa|\.gitlab/(issue|merge_request)_templates)/}
    UPDATE_GITALY_BRANCH = 'release-tools/update-gitaly'

    attr_reader :event

    def initialize(event)
      @event = event
    end

    def need_mr_approved_label?
      !expedited_mr? &&
        !event.source_branch_is?(UPDATE_GITALY_BRANCH) &&
        !event.target_branch_is_stable_branch? &&
        !changed_file_list.only_change?(SKIP_WHEN_CHANGES_ONLY_REGEX)
    end

    private

    def expedited_mr?
      event.label_names.include?(Labels::PIPELINE_EXPEDITED_LABEL) ||
        # TODO: Remove once the label is renamed to be scoped
        event.label_names.include?(Labels::PIPELINE_LEGACY_EXPEDITE_LABEL)
    end

    def changed_file_list
      @changed_file_list ||= Triage::ChangedFileList.new(event.project_id, event.iid)
    end
  end
end
