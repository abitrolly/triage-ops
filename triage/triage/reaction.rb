# frozen_string_literal: true

require_relative '../triage'

module Triage
  module Reaction
    module_function

    def add_comment(text, noteable_path = event.noteable_path, append_source_link:)
      path = "#{noteable_path}/notes"

      Reaction.post_request(path, { body: text }, append_source_link: append_source_link)
    end
    public :add_comment

    # note: this may give HTTP 404 errors if the emoji is already given.
    def add_emoji_award(emoji_name, note_id = event.id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/notes/#{note_id}/award_emoji"

      Reaction.post_request(path, { name: emoji_name }, append_source_link: false)
    end
    public :add_emoji_award

    def add_discussion(text, noteable_path = event.noteable_path, append_source_link:)
      path = "#{noteable_path}/discussions"

      Reaction.post_request(path, { body: text }, append_source_link: append_source_link)
    end
    public :add_discussion

    def append_discussion(text, discussion_id, noteable_path = event.noteable_path, append_source_link:)
      path = "#{noteable_path}/discussions/#{discussion_id}/notes"

      Reaction.post_request(path, { body: text }, append_source_link: append_source_link)
    end
    public :append_discussion

    def merge_merge_request(noteable_path = event.noteable_path)
      path = "#{noteable_path}/merge"

      Reaction.put_request(path, { merge_when_pipeline_succeeds: true })
    end
    public :merge_merge_request

    def update_discussion_comment(text, discussion_id, note_id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions/#{discussion_id}/notes/#{note_id}"

      Reaction.put_request(path, { body: text })
    end
    public :update_discussion_comment

    def resolve_discussion(discussion_id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions/#{discussion_id}"

      Reaction.put_request(path, { resolved: true })
    end
    public :resolve_discussion

    def unresolve_discussion(discussion_id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions/#{discussion_id}"

      Reaction.put_request(path, { resolved: false })
    end
    public :unresolve_discussion

    def self.post_request(path, params, append_source_link: false)
      add_automation_suffix(params) if append_source_link

      Triage.api_client.post(path, body: params) unless Triage.dry_run?

      "POST #{Triage.api_endpoint(:com)}#{path}, params: `#{params}`"
    end

    def self.put_request(path, params)
      Triage.api_client.put(path, body: params) unless Triage.dry_run?

      "PUT #{Triage.api_endpoint(:com)}#{path}, params: `#{params}`"
    end

    def self.add_automation_suffix(params)
      return unless params.key?(:body)

      source_path = get_source_path(caller_locations(1..5))
      return unless source_path

      params[:body] = <<~MARKDOWN.chomp
        #{params[:body]}

        *This message was [generated automatically](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations).
        You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/#{source_path}).*
      MARKDOWN
    end

    def self.get_source_path(locations)
      processor_location = locations.find { |location| location.path.match?(%r{/triage/(processor|job)/}) }
      return unless processor_location

      processor_location.path.partition('/triage/').last
    end
  end
end
