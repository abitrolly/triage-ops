# frozen_string_literal: true

require_relative 'gcp_log_formatter'

require 'ougai'

module Triage
  module Logging
    module_function

    def sucker_punch_logger
      @sucker_punch_logger ||= logger.child(name: 'sucker_punch')
    end

    def rack_logger
      @rack_logger ||= logger.child(name: 'rack')
    end

    def logger
      @logger ||= begin
        result = ::Ougai::Logger.new(io)
        result.level = ENV['DRY_RUN'].nil? ? Logger::INFO : Logger::DEBUG
        result.formatter = Triage::GcpLogFormatter.new
        result
      end
    end

    def io(new_io = nil)
      @io = new_io || @io || $stdout
    end
  end
end
