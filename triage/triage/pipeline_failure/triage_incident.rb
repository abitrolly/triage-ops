# frozen_string_literal: true

require_relative '../../../triage/triage'
require_relative '../../../triage/resources/ci_job'
require_relative '../../../lib/constants/labels'
require_relative './notified_channels'
require_relative 'pipeline_incident_finder'

module Triage
  module PipelineFailure
    class TriageIncident
      ROOT_CAUSE_LABELS = Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS
      POST_RETRY_JOB_URL_THRESHOLD = 10
      FAILURE_TRACE_SIZE_LIMIT = 50000 # Keep it processable by human
      RSPEC_DURATION_COMMENT_LIMIT = 800000

      FRONTEND_CHANNEL = 'frontend'

      attr_reader :event, :config

      def initialize(event:, config:, ci_jobs:)
        @event = event
        @config = config
        @ci_jobs = ci_jobs
      end

      def top_root_cause_label
        return ROOT_CAUSE_LABELS[:default] if ci_jobs.empty?

        # find the top root cause label preferably not master-broken::undetermined
        potential_root_cause_labels
          .tally
          .max_by { |label, count| label == ROOT_CAUSE_LABELS[:default] ? 0 : count }[0]
      end

      def top_group_label
        return if ci_jobs.empty? || potential_responsible_group_labels.empty?

        potential_responsible_group_labels
          .tally
          .max_by { |_label, count| count }[0]
      end

      def test_level_labels
        test_level_labels = []

        test_level_labels << Labels::RSPEC_TEST_LEVEL_LABELS[:unit] if failed_with_test_level?(test_level: 'unit')
        test_level_labels << Labels::RSPEC_TEST_LEVEL_LABELS[:integration] if failed_with_test_level?(test_level: 'integration')
        test_level_labels << Labels::RSPEC_TEST_LEVEL_LABELS[:migration] if failed_with_test_level?(test_level: 'migration')
        test_level_labels << Labels::RSPEC_TEST_LEVEL_LABELS[:system] if failed_with_test_level?(test_level: 'system')
        test_level_labels << Labels::RSPEC_TEST_LEVEL_LABELS[:e2e] if failed_with_test_level?(job_prefix: '', test_level: 'e2e')

        test_level_labels
      end

      def root_cause_analysis_comment
        return if ci_jobs.empty?

        [
          list_all_ci_jobs_comment,
          retry_pipeline_comment,
          incident_modifier_comment
        ].compact.join("\n\n").prepend("\n\n")
      end

      def attribution_comment
        attributed_jobs = ci_jobs.map do |ci_job|
          <<~MARKDOWN.chomp
            #{ci_job.attribution_message_markdown}
          MARKDOWN
        end.uniq.reject(&:empty?)

        message_body = attributed_jobs.empty? ? '' : attributed_jobs.join("\n\n").prepend("\n\n")
        attributed_slack_channel_info.present? ? message_body + attributed_slack_channel_info : message_body
      end

      def investigation_comment
        return if ci_jobs.empty?

        valid_summaries = ci_jobs.map(&:test_failure_summary_markdown).reject(&:empty?)

        return if valid_summaries.empty?

        valid_summaries.join("\n\n").prepend("\n\n").chomp
      end

      def duration_analysis_comment
        jobs_with_rspec_run_time_summary = ci_jobs.filter_map(&:rspec_run_time_summary_markdown)

        return if jobs_with_rspec_run_time_summary.empty?

        string_dump = ''
        index_exceeding_limit = jobs_with_rspec_run_time_summary.reduce(0) do |index, data|
          string_dump += data
          break index if string_dump.size > rspec_duration_comment_limit

          index + 1
        end

        jobs_with_rspec_run_time_summary[0...index_exceeding_limit]
          .join("\n\n")
          .prepend("\n\n")
          .delete_suffix("\n\n") # removes any trailing '\n\n'
      end

      def duplicate_incident_url
        return @duplicate_incident_url if defined?(@duplicate_incident_url)

        @duplicate_incident_url = nil

        duplicate_incident = previous_incidents.find do |previous_incident|
          # if the previous incident was closed as a duplicate of another incident
          # and the current incident's failed jobs are a subset of the previous incident's ones
          # we mark the current incident as a duplicate as well
          previous_failed_job_names = previous_incident.title.split('with ').last.split(', ')

          Set.new(failed_job_names).subset?(Set.new(previous_failed_job_names))
        end

        return unless duplicate_incident

        @duplicate_incident_url = closed_as_duplicate_source_url(duplicate_incident)
      end

      def previous_incidents
        @previous_incidents ||= PipelineIncidentFinder.new(incident_project_id: config.canonical_incident_project_id).incidents
      end

      def duplicate?
        !!duplicate_incident_url
      end

      def closeable?
        duplicate? || all_jobs_failed_with_transient_errors?
      end

      private

      attr_reader :ci_jobs

      def list_all_ci_jobs_comment
        return if ci_jobs.empty?

        ci_jobs.map do |ci_job|
          %(- #{ci_job.markdown_link}: ~"#{ci_job.failure_root_cause_label}".#{retry_job_comment(ci_job)})
        end.join("\n")
      end

      def retry_job_comment(ci_job)
        retry_job_web_url = retry_job_and_return_web_url_if_applicable(ci_job)
        return if retry_job_web_url.nil?

        " Retried at: #{retry_job_web_url}"
      end

      def retry_job_and_return_web_url_if_applicable(ci_job)
        return unless transient_error?(ci_job.failure_root_cause_label) && retry_jobs_individually?

        ci_job.retry.web_url
      end

      def transient_error?(failure_root_cause_label)
        config.transient_root_cause_labels.include?(failure_root_cause_label)
      end

      def retry_jobs_individually?
        # the benefit of retrying individual job is so we can post the retried job url
        # but if there are too many failed jobs
        # it's more practical to click `retry pipeline` and verify the overall pipeline status when all jobs finish
        ci_jobs.size < POST_RETRY_JOB_URL_THRESHOLD
      end

      def retry_pipeline_comment
        return unless all_jobs_failed_with_transient_errors?
        return if retry_jobs_individually?

        retry_pipeline_response = Triage.api_client.retry_pipeline(event.project_id, event.id)

        "Retried pipeline: #{retry_pipeline_response.web_url}"
      end

      def incident_modifier_comment
        return unless closeable?

        if duplicate?
          format(config.duplicate_command_body, { duplicate_incident_url: duplicate_incident_url })
        else
          format(config.close_command_body)
        end
      end

      def all_jobs_failed_with_transient_errors?
        return @all_jobs_failed_with_transient_errors if defined?(@all_jobs_failed_with_transient_errors)

        @all_jobs_failed_with_transient_errors = (potential_root_cause_labels.uniq - config.transient_root_cause_labels).empty?
      end

      def potential_responsible_group_labels
        @potential_responsible_group_labels ||= ci_jobs.flat_map(&:potential_responsible_group_labels)
      end

      def closed_as_duplicate_source_url(incident)
        duplicate_link = incident._links['closed_as_duplicate_of']

        return incident.web_url unless duplicate_link

        # duplicate_link is an API URL, so we need to fetch the web URL instead
        Triage.api_client.get(duplicate_link.gsub(%r{\Ahttps://.+/api/v4}, '')).web_url
      end

      def potential_root_cause_labels
        @potential_root_cause_labels ||= ci_jobs.map(&:failure_root_cause_label)
      end

      def attributed_slack_channel_info
        channels_markdown = NotifiedChannels.new(config, top_group_label).to_s
        return if channels_markdown.empty?

        <<~MARKDOWN.chomp.prepend("\n\n")
        **This incident is attributed to ~"#{top_group_label}" and posted in #{channels_markdown}.**
        MARKDOWN
      end

      def rspec_duration_comment_limit
        RSPEC_DURATION_COMMENT_LIMIT
      end

      def failed_with_test_level?(test_level:, job_prefix: 'rspec')
        failed_job_names.any?(/#{job_prefix}.*#{test_level}/)
      end

      def failed_job_names
        ci_jobs.map(&:name)
      end
    end
  end
end
