resource_rules:
  merge_requests:
    summaries:
      - name: Track ~"Leading Organization" merge requests for review response SLO
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Leading Organization Report
            summary: |
              Assignees should go through the list and monitor the ~"Leading Organization" merge requests in keeping with the 4 day review-response SLO: https://about.gitlab.com/handbook/engineering/workflow/code-review/#review-response-slo

              After processing each merge request, the checkbox on the left should be checked and a note added to **THIS** issue, quoting the merge request, explaining what action was taken, and why.

              When going through the list of merge requests, please do the following:

              - If the merge request is set to ~"workflow::ready for review" or ~"workflow::in review" and has a reviewer assigned:
                - Check if a GitLab team member has responded to the author within the last 4 business days
                - If there has been no recent response ping the reviewer in a comment to ask if they have capacity to review and link to the 4 day review-response SLO: https://about.gitlab.com/handbook/engineering/workflow/code-review/#review-response-slo
              - If the merge request is set to ~"workflow::ready for review" or ~"workflow::in review" but no reviewer is assigned:
                - Use https://gitlab-org.gitlab.io/gitlab-roulette/ or https://about.gitlab.com/handbook/product/product-categories/ to find a reviewer and assign the merge request to them.
              - If the merge request depends on the completion of work elsewhere, set ~"workflow::blocked".

              Overview of merge requests:

              - In dev (author should get back to us) - https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Ain%20dev
              - Ready for review (a team member should pick up the review) - https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Aready%20for%20review
              - In review (a team member is actively reviewing) - https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Ain%20review
              - Blocked (inactionable) - https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Ablocked

              For more info about the process, see https://about.gitlab.com/handbook/engineering/quality/contributor-success/#working-with-community-contributions.

              #{ short_team_summary(group_key: "contributor_success", title: "## Leading Organization Report") }

              ----

              Job URL: #{ENV['CI_JOB_URL']}

              *This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/#{ENV['TRIAGE_POLICY_FILE']}).*
        rules:
          - name: Leading Organization merge requests for review
            conditions:
              state: opened
              labels:
                - Community contribution
                - Leading Organization
              forbidden_labels:
                - "workflow::in dev" # author is working on the issue
                - "workflow::blocked"
            limits:
              oldest: 999
            actions:
              summarize:
                item: |
                  - [ ] idle: #{IdleMrHelper.days_since_last_human_update(resource)}d | `{{author}}` | #{resource[:web_url]}+ | #{resource[:labels].grep(/^(type::|devops::|group::|workflow::)/).map { |label| %(~"#{label}") }.join(' ')}
                summary: |
                  ### #{resource[:items].lines.size} ~"Leading Organization" merge requests in review

                  {{items}}
