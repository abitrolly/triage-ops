# frozen_string_literal: true

source "https://rubygems.org"

git_source(:github) { |repo_name| "https://github.com/#{repo_name}" }

gem "json", "~> 2.7.2"
gem "sentry-raven", require: false
gem "gitlab-triage", "~> 1.42.2"
gem "gitlab", "~> 4.19.0", require: false
gem 'retriable', '~> 3.1', '>= 3.1.2', require: false

# Picked because it's used in gitlab. Can use any which follows redirect
gem "httparty"

gem "graphql", "~> 2.0.17"
gem "graphql-client", "~> 0.18.0"

# Reactive
gem "mini_cache", "~> 1.1.0", require: false
gem "sucker_punch", "~> 3.2.0", require: false
gem "rack", require: false
gem "rack-requestid", "~> 0.2", require: false
gem "puma", require: false
gem "slack-messenger", require: false
gem "ougai", "~> 2.0.0", require: false
gem 'discordrb-webhooks', '~> 3.5', require: false
gem 'google-apis-sheets_v4', '~> 0.31.0'

group :development, :test do
  gem 'gitlab-styles', '~> 10.1.0', require: false
end

group :development do
  gem "guard-rspec"
  gem "guard"
  gem "lefthook", require: false
  gem "pry-byebug", "~> 3.10.1"
  gem "rack-console"
  gem "solargraph", "~> 0.50.0"
end

group :test do
  gem "rspec", "~> 3.13.0", require: false
  # Fixes an activesupport incompatibility of `#with`
  gem "rspec-mocks", "~> 3.13.1", require: false
  gem "rspec-parameterized", require: false
  gem "rspec_junit_formatter", require: false
  gem "rack-test", require: false
  gem "timecop", require: false
  gem "webmock", require: false
end

group :danger, optional: true do
  gem 'gitlab-dangerfiles', '~> 4.7.0', require: false
end

group :coverage, optional: true do
  gem 'simplecov', '~> 0.22.0', require: false
  gem 'simplecov-lcov', '~> 0.8.0', require: false
  gem 'simplecov-cobertura', '~> 2.1.0', require: false
  gem 'undercover', '~> 0.5.0', require: false
end
